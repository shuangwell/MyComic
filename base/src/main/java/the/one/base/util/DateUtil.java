package the.one.base.util;

import android.annotation.SuppressLint;
import android.os.ParcelFormatException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * @author The one
 * @date 2020/4/16 0016
 * @describe 日期工具类
 * @email 625805189@qq.com
 * @remark
 */
public class DateUtil {

    /**
     *           格式
     *
     *  yyyy    四位年
     *  yy      两位年
     *  MM      月份  始终两位
     *  M       月份
     *  dd      日期  始终两位
     *  d       日期
     *  HH      24小时制  始终两位
     *  H       24小时制
     *  hh      12小时制  始终两位
     *  h       12小时制
     *  mm      分钟  始终两位
     *  ss      秒  始终两位
     *
     */

    /**
     * 年月日 时分秒
     */
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    /**
     * 年月日
     */
    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    /**
     * 月日
     */
    public static final String MM_DD = "MM-dd";

    /**
     * 周
     */
    private static final String[] WEEKS = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};

    /**
     * 获取今天 年月日 格式的 String
     *
     * @return
     */
    public static String getTodayYMDString() {
        return dateToString(new Date(), YYYY_MM_DD);
    }

    /**
     * Date 转换成 年月日 格式的 String
     *
     * @param date
     * @return
     */
    public static String dateToYMDString(Date date) {
        return dateToString(date, YYYY_MM_DD);
    }

    /**
     * Date 转换成 String
     *
     * @param date   需要转换的日期
     * @param format 转换格式
     * @return
     */
    public static String dateToString(Date date, String format) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    /**
     * Date 转换成  Calendar
     * @param date
     * @return
     */
    public static Calendar dateToCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    /**
     * Calendar 转换成 Date
     * @param date
     * @return
     */
    public static Date calendarToDate(Date date) {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    /**
     * string格式的日期转换成long类型
     * @param dateStr 需要转换的日期
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static long stringToLong(String dateStr) {
        return stringToLong(dateStr,YYYY_MM_DD);
    }

    /**
     * string格式的日期转换成long类型
     * @param dateStr 需要转换的日期
     * @param format 格式
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static long stringToLong(String dateStr, String format) {
        Date date = new Date();
        try {
            date = new SimpleDateFormat(format).parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    /**
     * string格式的日期转换成Date类型
     * @param dateStr 需要转换的日期
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static Date stringToDate(String dateStr) {
        return stringToDate(dateStr,YYYY_MM_DD);
    }

    /**
     * string格式的日期转换成Date类型
     * @param dateStr 需要转换的日期
     * @param format 格式
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static Date stringToDate(String dateStr, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try {
            return simpleDateFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new ParcelFormatException(e.getMessage());
        }
    }

    /**
     * 获取 - 年
     * @param calendar
     * @return
     */
    public static int getYear(Calendar calendar) {
        return calendar.get(Calendar.YEAR);
    }

    /**
     * 获取 - 月
     * @param calendar
     * @return
     */
    public static int getMonth(Calendar calendar) {
        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 获取 - 日
     * @param calendar
     * @return
     */
    public static int getDay(Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取 - 周
     * @param calendar
     * @return
     */
    public static String getWeek(Calendar calendar) {
        return WEEKS[calendar.get(Calendar.DAY_OF_WEEK) - 1];
    }

    /**
     * @return 获取几天之内的的Date
     * @param amount 之后为整数 之前为负数
     */
    public static Date getNearestDayDate(int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, amount);
        return calendar.getTime();
    }

    /**
     * @return 获取几周之内的的Date
     * @param amount 之后为整数 之前为负数
     */
    public static Date getNearestWeekDate(int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.WEEK_OF_YEAR, amount);
        return calendar.getTime();
    }

    /**
     * @return 获取几个月之内的日期
     * @param amount 之后为整数 之前为负数
     */
    public static Date getNearestMonthDate(int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, amount);
        return calendar.getTime();
    }

    /**
     *
     * @return 获取几年之内的Date
     * @param amount 之后为整数 之前为负数
     */
    public static Date getNearestYearDate(int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, amount);
        return calendar.getTime();
    }

    /**
     * 获取某个年份的Date
     * @param year
     * @return
     */
    public static Date getYearDate(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        return calendar.getTime();
    }

    /**
     * 获得两个日期间距多少天
     * @param beginDate
     * @return
     */
    public static int getTimeDistance(String beginDate){
        return getTimeDistance(stringToDate(beginDate),new Date());
    }

    /**
     * 获得两个日期间距多少天
     * @param beginDate
     * @param endDate
     * @return
     */
    public static int getTimeDistance(String beginDate,String endDate){
        return getTimeDistance(stringToDate(beginDate),stringToDate(endDate));
    }

    /**
     * 获得两个日期间距多少天
     * @param beginDate
     * @param endDate
     * @return
     */
    public static int getTimeDistance(Date beginDate, Date endDate) {
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(beginDate);
        fromCalendar.set(Calendar.HOUR_OF_DAY, fromCalendar.getMinimum(Calendar.HOUR_OF_DAY));
        fromCalendar.set(Calendar.MINUTE, fromCalendar.getMinimum(Calendar.MINUTE));
        fromCalendar.set(Calendar.SECOND, fromCalendar.getMinimum(Calendar.SECOND));
        fromCalendar.set(Calendar.MILLISECOND, fromCalendar.getMinimum(Calendar.MILLISECOND));

        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(endDate);
        toCalendar.set(Calendar.HOUR_OF_DAY, fromCalendar.getMinimum(Calendar.HOUR_OF_DAY));
        toCalendar.set(Calendar.MINUTE, fromCalendar.getMinimum(Calendar.MINUTE));
        toCalendar.set(Calendar.SECOND, fromCalendar.getMinimum(Calendar.SECOND));
        toCalendar.set(Calendar.MILLISECOND, fromCalendar.getMinimum(Calendar.MILLISECOND));

        int dayDistance = (int) ((toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 3600 * 24));
        dayDistance = Math.abs(dayDistance);

        return dayDistance;
    }

}

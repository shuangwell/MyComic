package top.luqichuang.common.self;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.NetUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 14:52
 * @ver 1.0
 */
public abstract class CommonCallback implements Callback {

    private Source source;
    private String tag;
    private Request request;
    private Map<String, Object> map = new HashMap<>();
    private Map<String, Object> data = new HashMap<>();

    public CommonCallback() {
    }

    public CommonCallback(Source source, String tag) {
        this.source = source;
        this.tag = tag;
    }

    @Override
    public void onFailure(@NotNull Call call, @NotNull IOException e) {
        request = call.request();
        String url = request.url().toString();
        NetUtil.addErrorUrl(url);
        if (NetUtil.needReload(url, getReloadNum())) {
            NetUtil.startLoad(request, this);
        } else {
            onFailure(e.getMessage());
        }
    }

    @Override
    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
        String html;
        request = call.request();
        data.put("url", request.url().toString());
        initData(data);
        if (source != null) {
            html = getHtml(response, source.getCharsetName(tag));
            Request nextRequest = source.buildRequest(html, tag, data, map);
            if (nextRequest != null) {
                NetUtil.startLoad(nextRequest, this);
                return;
            }
        } else {
            html = getHtml(response, null);
        }
        try {
            onResponse(html, map);
        } catch (Exception e) {
            onFailure(e.getMessage());
        }
    }

    public static String getHtml(Response response, String charsetName) {
        String html;
        try {
            byte[] b = response.body().bytes();
            if (charsetName == null) {
                html = new String(b, "UTF-8");
            } else {
                html = new String(b, charsetName);
            }
        } catch (Exception e) {
            html = "";
        }
        return html;
    }

    public Source getSource() {
        return source;
    }

    public String getTag() {
        return tag;
    }

    protected void initData(Map<String, Object> data) {
    }

    protected int getReloadNum() {
        return 1;
    }

    public abstract void onFailure(String errorMsg);

    public abstract void onResponse(String html, Map<String, Object> map);

}

package top.luqichuang.common.source.comic;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.json.JsonNode;
import top.luqichuang.common.json.JsonStarter;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.model.comic.ComicInfo;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/7/21 13:26
 * @ver 1.0
 */
@Deprecated
public class QiXi extends BaseComicSource {
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.QI_XI;
    }

    @Override
    public Map<String, String> getImageHeaders() {
        return null;
    }

    @Override
    public String getIndex() {
        return "http://www.qiximh4.com";
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = getIndex() + "/search?keyword=" + searchString;
        return NetUtil.getRequest(url);
    }

    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (DETAIL.equals(tag) && map.isEmpty()) {
            if (data.get("oUrl") == null) {
                data.put("oUrl", data.get("url"));
                String id = null;
                try {
                    String url = (String) data.get("url");
                    url = StringUtil.remove(url, getIndex());
                    id = StringUtil.remove(url, "/");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return NetUtil.postRequest(getIndex() + "/chapterlist/", "id", id);
            } else {
                map.put("moreChapter", html);
                map.put("url", data.get("oUrl"));
                return NetUtil.getRequest((String) data.get("oUrl"));
            }
        }
        return null;
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                String title = node.ownText("li.title a");
                String author = null;
                String updateTime = null;
                String updateChapter = null;
                String imgUrl = node.src("img");
                String detailUrl = getIndex() + node.href("a");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        return starter.startElements(html, "div.cy_list_mh ul");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("div.cy_title h1");
                String imgUrl = node.attr("div.cy_info_cover img", "data-src");
                String author = node.ownText("div.cy_xinxi span");
                String intro = node.ownText("p#comic-description");
                String updateStatus = node.ownText("div.cy_xinxi span:eq(1)");
                String updateTime = node.ownText("span.looking_chapter");
                author = StringUtil.remove(author, "作者：");
                updateStatus = StringUtil.remove(updateStatus, "状态：");
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        List<ChapterInfo> list = starter.startElements(html, "div.cy_plist li");
        String moreChapter = (String) map.get("moreChapter");
        if (moreChapter != null) {
            JsonStarter<ChapterInfo> jsonStarter = new JsonStarter<ChapterInfo>() {
                @Override
                protected ChapterInfo dealDataList(JsonNode node) {
                    String title = node.string("name");
                    String chapterUrl = map.get("url") + "/" + node.string("id") + ".html";
                    return new ChapterInfo(title, chapterUrl);
                }
            };
            list.addAll(jsonStarter.startDataList(moreChapter, "data", "list"));
        }
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, list);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String[] urls = null;
        try {
            String js = StringUtil.matchLast("(eval\\(function.*?\\{\\}\\)\\))", html);
            if (js != null) {
                String[] ss = {"smkhy258", "smkd95fv", "md496952", "cdcsdwq", "vbfsa256", "cawf151c", "cd56cvda", "8kihnt9", "dso15tlo", "5ko6plhy"};
                int dataId = Integer.parseInt(StringUtil.match("data-id=\"(\\d)\"", html));
                String code1 = DecryptUtil.decryptPackedJsCode(js);
                String code2 = ss[dataId];
                code1 = StringUtil.match("=\"(.*?)\"", code1);
                code1 = DecryptUtil.decryptBase64(code1);
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < code1.length(); i++) {
                    builder.append((char) (code1.charAt(i) ^ code2.charAt(i % code2.length())));
                }
                String result = builder.toString();
                result = DecryptUtil.decryptBase64(result);
                if (result != null) {
                    result = result
                            .replace("\\u0026", "&")
                            .replace("\"", "")
                            .replace("[", "")
                            .replace("]", "");
                    urls = result.split(",");
                }
            }
        } catch (Exception ignored) {
        }
        return SourceHelper.getContentList(urls, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<ul><li><a href=\"/rank/1-1.html\">人气榜</a></li><li><a href=\"/rank/2-1.html\">周读榜</a></li><li><a href=\"/rank/3-1.html\">月读榜</a></li><li><a href=\"/rank/4-1.html\">火爆榜</a></li><li><a href=\"/rank/5-1.html\">更新榜</a></li><li><a href=\"/rank/6-1.html\">新慢榜</a></li></ul><ul><li><a href=\"/sort/1-1.html\">冒险热血</a></li><li><a href=\"/sort/2-1.html\">武侠格斗</a></li><li><a href=\"/sort/3-1.html\">科幻魔幻</a></li><li><a href=\"/sort/4-1.html\">侦探推理</a></li><li><a href=\"/sort/5-1.html\">耽美爱情</a></li><li><a href=\"/sort/6-1.html\">生活漫画</a></li><li><a href=\"/sort/12-1.html\">完结漫画</a></li><li><a href=\"/sort/13-1.html\">连载漫画</a></li></ul>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }
}

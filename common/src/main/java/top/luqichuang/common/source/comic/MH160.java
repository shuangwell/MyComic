package top.luqichuang.common.source.comic;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2023/1/27 14:04
 * @ver 1.0
 */
public class MH160 extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.MH_160;
    }

    @Override
    public String getIndex() {
        return "https://www.mh160.cc";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/statics/searchelxt1e1.aspx?key=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                String updateTime = node.ownText("p.mh-up-time");
                updateTime = StringUtil.remove(updateTime, "最后更新时间：");
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("h4 a"))
                        .buildAuthor(null)
                        .buildUpdateTime(updateTime)
                        .buildUpdateChapter(null)
                        .buildImgUrl(node.src("img"))
                        .buildDetailUrl(getIndex() + node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "ul.mh-search-list li");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected void dealInfo(JsoupNode node) {
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("h4 a"))
                        .buildAuthor(node.ownText("span.one a"))
                        .buildIntro(node.ownText("div#workint p"))
                        .buildUpdateTime(node.ownText("div.cy_zhangjie_top font"))
                        .buildUpdateStatus(node.ownText("p.works-info-tc span:eq(3) em"))
                        .buildImgUrl(node.src("div.mh-date-bgpic img"))
                        .buildChapterInfoList(startElements(html, "ul#mh-chapter-list-ol-0 li"))
                        .build();
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("p");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        List<String> urlList = new ArrayList<>();
        try {
            String server = "https://mhpic789-5.kingwar.cn/";
            String js = StringUtil.match("qTcms_S_m_murl_e=\"(.*?)\"", html);
            js = DecryptUtil.decryptBase64(js);
            String[] pics = js.split("\\$qingtiandy\\$");
            for (String pic : pics) {
                urlList.add(server + pic);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return SourceHelper.getContentList(urlList, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<div class=\"mh-tags-box\"><a title=\"热血\"href=\"/kanmanhua/rexue/\"class=\"classid111\">热血</a><a title=\"格斗\"href=\"/kanmanhua/gedou/\"class=\"classid112\">格斗</a><a title=\"科幻\"href=\"/kanmanhua/kehuan/\"class=\"classid113\">科幻</a><a title=\"搞笑\"href=\"/kanmanhua/gaoxiao/\"class=\"classid115\">搞笑</a><a title=\"推理\"href=\"/kanmanhua/tuili/\"class=\"classid116\">推理</a><a title=\"恐怖\"href=\"/kanmanhua/kongbu/\"class=\"classid117\">恐怖</a><a title=\"耽美\"href=\"/kanmanhua/danmei/\"class=\"classid118\">耽美</a><a title=\"少女\"href=\"/kanmanhua/shaonv/\"class=\"classid119\">少女</a><a title=\"恋爱\"href=\"/kanmanhua/lianai/\"class=\"classid120\">恋爱</a><a title=\"生活\"href=\"/kanmanhua/shenghuo/\"class=\"classid121\">生活</a><a title=\"战争\"href=\"/kanmanhua/zhanzheng/\"class=\"classid122\">战争</a><a title=\"其他\"href=\"/kanmanhua/qita/\"class=\"classid168\">其他</a></div>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a") + "%d.html");
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }
}

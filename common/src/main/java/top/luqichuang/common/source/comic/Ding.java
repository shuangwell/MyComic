package top.luqichuang.common.source.comic;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.ChapterInfoBuilder;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.starter.ElementNode;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2024/3/30 19:17
 * @ver 1.0
 */
public class Ding extends BaseComicSource {
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.DING;
    }

    @Override
    public String getIndex() {
        return "https://www.dingmanhua.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search/?q=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (CONTENT.equals(tag) && map.isEmpty()) {
            ElementNode node = new ElementNode(html);
            map.put("size", node.getElements("img.pic_comic").size());
            String code = StringUtil.match("<script src=\"(/chapter/.*?)\">", html);
            String url = getIndex() + code;
            return NetUtil.getRequest(url);
        }
        return super.buildRequest(html, tag, data, map);
    }

    protected InfoListConfig getInfoListConfig(ElementNode node) {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"div.row > div"};
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(node.ownText("div.card-text"))
                                .buildAuthor(null)
                                .buildUpdateTime(null)
                                .buildUpdateChapter(null)
                                .buildImgUrl(node.attr("img", "data-src"))
                                .buildDetailUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    protected InfoDetailConfig getInfoDetailConfig(ElementNode node) {
        return new InfoDetailConfig() {
            @Override
            protected void setData(EntityInfo info, ElementNode node) {
                String updateTime = node.ownText("span.ms-auto");
                updateTime = StringUtil.remove(updateTime, "更新于：");
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("p.fs-4"))
                        .buildAuthor(node.ownText("div.author-content a"))
                        .buildIntro(node.ownText("div.summary-content"))
                        .buildUpdateTime(updateTime)
                        .buildUpdateStatus(node.ownText("div.status-content a"))
                        .buildImgUrl(node.attr("div.comic_img img", "data-src"))
                        .build();
            }

            @Override
            protected void addListConfig(List<ListConfig<ChapterInfo>> configList) {
                configList.add(new ListConfig<ChapterInfo>() {
                    @Override
                    protected boolean needSwap(List<ChapterInfo> list) {
                        return true;
                    }

                    @Override
                    protected String[] getQuery() {
                        return new String[]{"div#chapter-list > div"};
                    }

                    @Override
                    protected ChapterInfo getData(ElementNode node) {
                        return new ChapterInfoBuilder()
                                .buildTitle(node.ownText("a"))
                                .buildUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    protected ContentListConfig getContentListConfig(ElementNode node) {
        return new ContentListConfig() {
            @Override
            protected void addUrlList(ElementNode node, List<String> urlList, Map<String, Object> map) {
                String comicUrl = StringUtil.match("comic_url = '(.*?)';", node.getHtml());
                String path = StringUtil.match("path = '(.*?)';", node.getHtml());
                String chapterName = StringUtil.match("chapter_name = '(.*?)';", node.getHtml());
                int size = 0;
                if (map.get("size") != null) {
                    size = (int) map.get("size");
                }
                for (int i = 0; i < size; i++) {
                    String url = String.format(Locale.CHINA, "%s/%s/%s/%d.webp", comicUrl, path, chapterName, (i + 1));
                    urlList.add(url);
                }
            }
        };
    }

    protected RankMapConfig getRankMapConfig() {
        return new RankMapConfig() {
            @Override
            protected String getHtml() {
                return "<div class=\"border-bottom pb-2 px-1\"><div class=\"category-content my-2\"><span class=\"text-lighter\">分类：</span><a href=\"?category=&amp;tag=\"class=\"btn btn-sm btn-outline-danger active\">全部</a><a href=\"?category=1&amp;tag=\"class=\"btn btn-sm btn-outline-danger \">日本漫画</a><a href=\"?category=3&amp;tag=\"class=\"btn btn-sm btn-outline-danger \">大陆漫画</a><a href=\"?category=4&amp;tag=\"class=\"btn btn-sm btn-outline-danger \">韩国漫画</a></div><div class=\"tag-content my-2\"><span class=\"text-lighter\">标签：</span><a href=\"?category=&amp;tag=1\"class=\"btn btn-sm btn-outline-danger \">悬疑</a><a href=\"?category=&amp;tag=2\"class=\"btn btn-sm btn-outline-danger \">欢乐</a><a href=\"?category=&amp;tag=3\"class=\"btn btn-sm btn-outline-danger \">搞笑</a><a href=\"?category=&amp;tag=4\"class=\"btn btn-sm btn-outline-danger \">玄幻</a><a href=\"?category=&amp;tag=5\"class=\"btn btn-sm btn-outline-danger \">冒险</a><a href=\"?category=&amp;tag=6\"class=\"btn btn-sm btn-outline-danger \">爱情</a><a href=\"?category=&amp;tag=7\"class=\"btn btn-sm btn-outline-danger \">百合</a><a href=\"?category=&amp;tag=8\"class=\"btn btn-sm btn-outline-danger \">推理</a><a href=\"?category=&amp;tag=9\"class=\"btn btn-sm btn-outline-danger \">热血</a><a href=\"?category=&amp;tag=10\"class=\"btn btn-sm btn-outline-danger \">异界</a><a href=\"?category=&amp;tag=11\"class=\"btn btn-sm btn-outline-danger \">轻改</a><a href=\"?category=&amp;tag=12\"class=\"btn btn-sm btn-outline-danger \">奇幻</a><a href=\"?category=&amp;tag=13\"class=\"btn btn-sm btn-outline-danger \">校园</a><a href=\"?category=&amp;tag=14\"class=\"btn btn-sm btn-outline-danger \">妹控</a><a href=\"?category=&amp;tag=15\"class=\"btn btn-sm btn-outline-danger \">生活</a><a href=\"?category=&amp;tag=16\"class=\"btn btn-sm btn-outline-danger \">竞技</a><a href=\"?category=&amp;tag=17\"class=\"btn btn-sm btn-outline-danger \">同人</a><a href=\"?category=&amp;tag=18\"class=\"btn btn-sm btn-outline-danger \">伪娘</a><a href=\"?category=&amp;tag=19\"class=\"btn btn-sm btn-outline-danger \">东方</a><a href=\"?category=&amp;tag=20\"class=\"btn btn-sm btn-outline-danger \">美食</a><a href=\"?category=&amp;tag=21\"class=\"btn btn-sm btn-outline-danger \">格斗</a><a href=\"?category=&amp;tag=22\"class=\"btn btn-sm btn-outline-danger \">战争</a><a href=\"?category=&amp;tag=23\"class=\"btn btn-sm btn-outline-danger \">舰娘</a><a href=\"?category=&amp;tag=24\"class=\"btn btn-sm btn-outline-danger \">治愈</a><a href=\"?category=&amp;tag=25\"class=\"btn btn-sm btn-outline-danger \">魔幻</a><a href=\"?category=&amp;tag=26\"class=\"btn btn-sm btn-outline-danger \">职场</a><a href=\"?category=&amp;tag=27\"class=\"btn btn-sm btn-outline-danger \">性转</a><a href=\"?category=&amp;tag=28\"class=\"btn btn-sm btn-outline-danger \">萌系</a><a href=\"?category=&amp;tag=29\"class=\"btn btn-sm btn-outline-danger \">后宫</a><a href=\"?category=&amp;tag=30\"class=\"btn btn-sm btn-outline-danger \">节操</a><a href=\"?category=&amp;tag=31\"class=\"btn btn-sm btn-outline-danger \">魔法</a><a href=\"?category=&amp;tag=32\"class=\"btn btn-sm btn-outline-danger \">科幻</a><a href=\"?category=&amp;tag=33\"class=\"btn btn-sm btn-outline-danger \">穿越</a><a href=\"?category=&amp;tag=34\"class=\"btn btn-sm btn-outline-danger \">仙侠</a><a href=\"?category=&amp;tag=35\"class=\"btn btn-sm btn-outline-danger \">都市</a><a href=\"?category=&amp;tag=36\"class=\"btn btn-sm btn-outline-danger \">异能</a><a href=\"?category=&amp;tag=37\"class=\"btn btn-sm btn-outline-danger \">网游</a><a href=\"?category=&amp;tag=38\"class=\"btn btn-sm btn-outline-danger \">短篇</a><a href=\"?category=&amp;tag=39\"class=\"btn btn-sm btn-outline-danger \">女主</a><a href=\"?category=&amp;tag=40\"class=\"btn btn-sm btn-outline-danger \">恐怖</a><a href=\"?category=&amp;tag=41\"class=\"btn btn-sm btn-outline-danger \">重生</a><a href=\"?category=&amp;tag=42\"class=\"btn btn-sm btn-outline-danger \">日常</a><a href=\"?category=&amp;tag=43\"class=\"btn btn-sm btn-outline-danger \">侦探</a><a href=\"?category=&amp;tag=44\"class=\"btn btn-sm btn-outline-danger \">言情</a><a href=\"?category=&amp;tag=45\"class=\"btn btn-sm btn-outline-danger \">生存</a><a href=\"?category=&amp;tag=46\"class=\"btn btn-sm btn-outline-danger \">武侠</a><a href=\"?category=&amp;tag=47\"class=\"btn btn-sm btn-outline-danger \">系统</a><a href=\"?category=&amp;tag=48\"class=\"btn btn-sm btn-outline-danger \">励志</a><a href=\"?category=&amp;tag=49\"class=\"btn btn-sm btn-outline-danger \">末世</a><a href=\"?category=&amp;tag=50\"class=\"btn btn-sm btn-outline-danger \">转生</a><a href=\"?category=&amp;tag=51\"class=\"btn btn-sm btn-outline-danger \">长篇</a><a href=\"?category=&amp;tag=52\"class=\"btn btn-sm btn-outline-danger \">修仙</a><a href=\"?category=&amp;tag=53\"class=\"btn btn-sm btn-outline-danger \">末日</a><a href=\"?category=&amp;tag=54\"class=\"btn btn-sm btn-outline-danger \">灵异</a><a href=\"?category=&amp;tag=55\"class=\"btn btn-sm btn-outline-danger \">游戏</a><a href=\"?category=&amp;tag=56\"class=\"btn btn-sm btn-outline-danger \">爱倩</a><a href=\"?category=&amp;tag=57\"class=\"btn btn-sm btn-outline-danger \">销毁</a><a href=\"?category=&amp;tag=58\"class=\"btn btn-sm btn-outline-danger \">病娇</a><a href=\"?category=&amp;tag=59\"class=\"btn btn-sm btn-outline-danger \">音乐</a><a href=\"?category=&amp;tag=60\"class=\"btn btn-sm btn-outline-danger \">爆笑</a><a href=\"?category=&amp;tag=61\"class=\"btn btn-sm btn-outline-danger \">机战</a><a href=\"?category=&amp;tag=62\"class=\"btn btn-sm btn-outline-danger \">复仇</a><a href=\"?category=&amp;tag=63\"class=\"btn btn-sm btn-outline-danger \">舞蹈</a><a href=\"?category=&amp;tag=64\"class=\"btn btn-sm btn-outline-danger \">战斗</a><a href=\"?category=&amp;tag=65\"class=\"btn btn-sm btn-outline-danger \">逆袭</a><a href=\"?category=&amp;tag=66\"class=\"btn btn-sm btn-outline-danger \">历史</a><a href=\"?category=&amp;tag=67\"class=\"btn btn-sm btn-outline-danger \">恋爱</a><a href=\"?category=&amp;tag=68\"class=\"btn btn-sm btn-outline-danger \">养成</a><a href=\"?category=&amp;tag=69\"class=\"btn btn-sm btn-outline-danger \">四格</a><a href=\"?category=&amp;tag=70\"class=\"btn btn-sm btn-outline-danger \">宫廷</a><a href=\"?category=&amp;tag=71\"class=\"btn btn-sm btn-outline-danger \">诡异</a></div></div>";
            }

            @Override
            protected String getCssQuery() {
                return "a";
            }

            @Override
            protected String getTitle(ElementNode node) {
                return node.ownText("a");
            }

            @Override
            protected String getUrl(ElementNode node) {
                return String.format("%s/sort/%s", getIndex(), node.href("a")) + "&page=%d";
            }
        };
    }
}

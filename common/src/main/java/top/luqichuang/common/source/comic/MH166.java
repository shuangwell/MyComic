package top.luqichuang.common.source.comic;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2024/3/5 14:49
 * @ver 1.0
 */
@Deprecated
public class MH166 extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.MH_166;
    }

    @Override
    public String getIndex() {
        return "https://www.manhua166.com";
    }

    @Override
    public boolean isValid() {
        return false;//error index
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search/?keywords=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                String updateChapter = node.ownText("span.tt");
                updateChapter = StringUtil.remove(updateChapter, "更新至");
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.title("dt a"))
                        .buildAuthor(null)
                        .buildUpdateTime(node.ownText("span font"))
                        .buildUpdateChapter(updateChapter)
                        .buildImgUrl(node.src("img"))
                        .buildDetailUrl(node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "div#dmList li");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("div.position strong"))
                        .buildAuthor(node.ownText("div.info p:eq(1)"))
                        .buildIntro(node.ownText("div#intro1 p"))
                        .buildUpdateTime(node.ownText("div.info span.red"))
                        .buildUpdateStatus(null)
                        .buildImgUrl(node.src("img.pic"))
                        .buildChapterInfoList(startElements(html, "ul#chapter-list-1 li"))
                        .build();
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String[] urls = null;
        String chapterImages = StringUtil.match("chapterImages = \\[(.*?)\\];", html);
        if (chapterImages != null) {
            chapterImages = chapterImages.replace("\"", "").replace("\\", "");
            urls = chapterImages.split(",");
        }
        return SourceHelper.getContentList(urls, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<ul class=\"nav_menu\"><li><a href=\"/list/rexue/\"title=\"热血\">少年热血</a></li><li><a href=\"/list/wuxia/\"title=\"格斗\">武侠格斗</a></li><li><a href=\"/list/kehuan/\"title=\"科幻\">科幻魔幻</a></li><li><a href=\"/list/tuili/\"title=\"推理\">侦探推理</a></li><li><a href=\"/list/kongbu/\"title=\"恐怖\">恐怖灵异</a></li><li><a href=\"/list/danmei/\"title=\"耽美\">耽美人生</a></li><li><a href=\"/list/shaonv/\"title=\"少女\">少女爱情</a></li><li><a href=\"/list/lianai/\"title=\"恋爱\">恋爱生活</a></li><li><a href=\"/list/shenghuo/\"title=\"生活\">生活漫画</a></li><li><a href=\"/list/zhanzheng/\"title=\"战争\">战争漫画</a></li></ul>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a") + "%d/");
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }
}

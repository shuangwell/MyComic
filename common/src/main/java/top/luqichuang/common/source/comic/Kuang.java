package top.luqichuang.common.source.comic;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2024/3/8 15:50
 * @ver 1.0
 */
public class Kuang extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.KUANG;
    }

    @Override
    public String getIndex() {
        return "https://www.cartoonmad.com";
    }

    @Override
    public String getCharsetName(String tag) {
        return "big5";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search.html", getIndex());
        String keyword = DecryptUtil.getEncodeStr(StringUtil.convertToTraditional(searchString), "big5");
        String raw = String.format("keyword=%s&searchtype=all", keyword);
        return NetUtil.postRequest(url, raw);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        String simpleHtml = StringUtil.convertToSimple(html);
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                String updateChapter = node.ownText("a.a2");
                updateChapter = StringUtil.remove(updateChapter, "更新到");
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.title("td>a"))
                        .buildAuthor(null)
                        .buildUpdateTime(null)
                        .buildUpdateChapter(updateChapter)
                        .buildImgUrl(getIndex() + node.src("img"))
                        .buildDetailUrl(getIndex() + "/" + node.href("td>a"))
                        .build();
            }
        };
        return starter.startElements(simpleHtml, "table[cellpadding='4']");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        String simpleHtml = StringUtil.convertToSimple(html);
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String author = node.ownText("td[width='300'][height='24']", 1);
                author = StringUtil.remove(author, "原创作者： ");
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("td[style='font-size:12pt;color:#000066'] a", 2))
                        .buildAuthor(author)
                        .buildIntro(node.ownText("td[style='font-size: 11pt ; line-height:20pt; font-family:Microsoft YaHei; ']"))
                        .buildUpdateTime(null)
                        .buildUpdateStatus(null)
                        .buildImgUrl(getIndex() + node.src("td[bgcolor='#F5F5F5'] img"))
                        .buildChapterInfoList(startElements(simpleHtml, "table[width='800'][align='center'] td:not([width]):not([height])"))
                        .build();
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(simpleHtml);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        JsoupNode node = new JsoupNode(html);
        String src = node.src("td[align='center']>a>img");
        String baseUrl = "https:" + src;
        baseUrl = baseUrl.replace("001.", "%03d.");
        int size = node.getElements("option[value]").size();
        String[] urls = new String[size];
        for (int i = 0; i < size; i++) {
            urls[i] = String.format(baseUrl, i + 1);
        }
        return SourceHelper.getContentList(urls, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<tr align=\"center\"valign=\"top\"><td width=\"148\"background=\"/image/memul.png\"><a class=\"tmemu\"style=\"font-size:15px;font-family:'微软正黑体';\"href=\"/hotrank.html\"><b>热门连载</b></a></td><td width=\"148\"background=\"/image/memul.png\"><a class=\"tmemu\"style=\"font-size:15px;font-family:'微软正黑体';\"href=\"/topcm.html\"><b>网友推荐</b></a></td><td width=\"149\"background=\"/image/memul.png\"><a class=\"tmemu\"style=\"font-size:15px;font-family:'微软正黑体';\"href=\"/newcm.html\"><b>最新上架</b></a></td></tr><tbody><tr><td height=\"2\"colspan=\"4\"></td></tr><tr><td height=\"20\"align=\"center\"><a href=\"/comic01.html\">格斗</a></td><td align=\"center\"><a href=\"/comic02.html\">魔法</a></td><td align=\"center\"><a href=\"/comic03.html\">侦探</a></td><td align=\"center\"><a href=\"/comic04.html\">竞技</a></td></tr><tr><td height=\"20\"align=\"center\"><a href=\"/comic10.html\">恐怖</a></td><td align=\"center\"><a href=\"/comic07.html\">战国</a></td><td align=\"center\"><a href=\"/comic08.html\">魔幻</a></td><td align=\"center\"><a href=\"/comic09.html\">冒险</a></td></tr><tr><td height=\"20\"align=\"center\"><a href=\"/comic16.html\">校园</a></td><td align=\"center\"><a href=\"/comic17.html\">搞笑</a></td><td align=\"center\"><a href=\"/comic13.html\">少女</a></td><td align=\"center\"><a href=\"/comic14.html\">少男</a></td></tr><tr><td height=\"20\"align=\"center\"><a href=\"/comic18.html\">科幻</a></td><td align=\"center\"><a href=\"/comic21.html\">港产</a></td><td align=\"center\"><a href=\"/comic22.html\">其他</a></td><td align=\"center\">&nbsp;</td></tr></tbody>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.text("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }
}
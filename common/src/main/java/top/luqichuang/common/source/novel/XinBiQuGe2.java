package top.luqichuang.common.source.novel;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.NSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.novel.BaseNovelSource;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/7/27 9:30
 * @ver 1.0
 */
public class XinBiQuGe2 extends BaseNovelSource {
    @Override
    public NSourceEnum getNSourceEnum() {
        return NSourceEnum.XIN_BI_QU_GE_2;
    }

    @Override
    public String getIndex() {
        return "https://www.ibiquge.la";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = getIndex() + "/modules/article/waps.php";
        return NetUtil.postRequest(url, "searchkey", searchString);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("td.even a"))
                        .buildAuthor(node.ownText("td.even", 1))
                        .buildUpdateTime(node.ownText("td.odd", 1))
                        .buildUpdateChapter(null)
                        .buildImgUrl(null)
                        .buildDetailUrl(node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "div#main tr");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String author = node.ownText("div#info p");
                String updateTime = node.ownText("div#info p", 2);
                author = StringUtil.remove(author, "作 者：");
                updateTime = StringUtil.remove(updateTime, "最后更新：");
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("div#info h1"))
                        .buildAuthor(author)
                        .buildIntro(node.ownText("div#intro p", 1))
                        .buildUpdateTime(updateTime)
                        .buildUpdateStatus(null)
                        .buildImgUrl(node.src("div#fmimg img"))
                        .buildChapterInfoList(startElements(html, "div#list dd"))
                        .build();
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        JsoupNode node = new JsoupNode(html);
        String content = node.remove("p").html("div#content");
        content = SourceHelper.getCommonContent(content);
        return SourceHelper.getContentList(new Content(chapterId, content));
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<ul>\t<li><a href=\"/xuanhuanxiaoshuo/\">玄幻小说</a></li>\t<li><a href=\"/xiuzhenxiaoshuo/\">修真小说</a></li>\t<li><a href=\"/dushixiaoshuo/\">都市小说</a></li>\t<li><a href=\"/chuanyuexiaoshuo/\">穿越小说</a></li>\t<li><a href=\"/wangyouxiaoshuo/\">网游小说</a></li>\t<li><a href=\"/kehuanxiaoshuo/\">科幻小说</a></li></ul>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("span.s2 a"))
                        .buildAuthor(node.ownText("span.s5"))
                        .buildUpdateTime(null)
                        .buildUpdateChapter(null)
                        .buildImgUrl(null)
                        .buildDetailUrl(node.href("span.s2 a"))
                        .build();
            }
        };
        return starter.startElements(html, "div.r li");
    }
}

package top.luqichuang.common.source.novel;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.NSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.novel.BaseNovelSource;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/12 16:14
 * @ver 1.0
 */
public class XiaoShuoE extends BaseNovelSource {
    @Override
    public NSourceEnum getNSourceEnum() {
        return NSourceEnum.XIAO_SHUO_E;
    }

    @Override
    public String getIndex() {
        //https://www.eexiaoshuo.cc
        return "https://www.qu-la.com";
    }

    @Override
    public String getCharsetName(String tag) {
        if (SEARCH.equals(tag)) {
            return "UTF-8";
        }
        return "GBK";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("https://so.biqusoso.com/s.php?q=%s", DecryptUtil.getGBKEncodeStr(searchString));
        return NetUtil.getRequest(url);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("span.s2 a"))
                        .buildAuthor(node.ownText("span.s4"))
                        .buildUpdateTime(null)
                        .buildUpdateChapter(null)
                        .buildImgUrl(null)
                        .buildDetailUrl(node.href("span.s2 a"))
                        .build();
            }
        };
        return starter.startElements(html, "div.search-list li");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String author = node.ownText("div.book-text span");
                author = StringUtil.remove(author, " 著");
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("div.book-text h1"))
                        .buildAuthor(author)
                        .buildIntro(node.text("div.intro"))
                        .buildUpdateTime(node.ownText("p.update-text span"))
                        .buildUpdateStatus(node.ownText("div.tag span.red"))
                        .buildImgUrl(getIndex() + node.src("div#fengmian img"))
                        .buildChapterInfoList(startElements(html, "ul.cf:eq(3) li"))
                        .build();
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        JsoupNode node = new JsoupNode(html);
        String content = node.remove("a").html("div#txt");
        content = SourceHelper.getCommonContent(content, "<br>");
        return SourceHelper.getContentList(new Content(chapterId, content));
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<ul><li class=\"state\"><a href=\"/class1/\">玄幻小说</a></li><li><a href=\"/class2/\">修真小说</a></li><li><a href=\"/class3/\">都市小说</a></li><li><a href=\"/class4/\">穿越小说</a></li><li><a href=\"/class5/\">网游小说</a></li><li><a href=\"/class6/\">科幻小说</a></li><li><a href=\"/class7/\">其他小说</a></li><li><a href=\"/finish/\">完本小说</a></li></ul>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("div.book-info a"))
                        .buildAuthor(node.ownText("div.book-info p"))
                        .buildUpdateTime(null)
                        .buildUpdateChapter(null)
                        .buildImgUrl(getIndex() + node.src("div.book-img img"))
                        .buildDetailUrl(getIndex() + node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "div.all-book-list li");
    }
}

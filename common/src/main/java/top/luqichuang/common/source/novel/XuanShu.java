package top.luqichuang.common.source.novel;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.NSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.novel.BaseNovelSource;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/2/16 19:48
 * @ver 1.0
 */
public class XuanShu extends BaseNovelSource {
    @Override
    public NSourceEnum getNSourceEnum() {
        return NSourceEnum.XUAN_SHU;
    }

    @Override
    public String getIndex() {
        return "https://www.ibiquta.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = getIndex() + "/search.html";
        return NetUtil.postRequest(url, "searchkey", searchString);
    }

    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (DETAIL.equals(tag) && map.isEmpty()) {
            JsoupNode node = new JsoupNode(html);
            String url = node.href("li.downAddress_li:eq(1) a");
            map.put("url", url);
            String imgUrl = node.src("dd.downInfoRowL img");
            String intro = node.ownText("div#mainSoftIntro p");
            map.put("imgUrl", imgUrl);
            map.put("intro", intro);
            return NetUtil.getRequest(url);
        }
        return null;
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                String author = node.ownText("a");
                try {
                    author = author.split("作者：")[1];
                } catch (Exception ignored) {
                }
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.title("a"))
                        .buildAuthor(author)
                        .buildUpdateTime(null)
                        .buildUpdateChapter(null)
                        .buildImgUrl(null)
                        .buildDetailUrl(node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "div#searchmain div.searchTopic");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("div.view_t");
                String author = node.ownText("div.view_info");
                String updateTime = node.ownText("div.view_info");
                try {
                    title = title.split("_")[0];
                    author = author.split(":")[1].split(" ")[0];
                    updateTime = updateTime.split("上传时间:")[1];
                } catch (Exception ignored) {
                }
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(title)
                        .buildAuthor(author)
                        .buildIntro((String) map.get("intro"))
                        .buildUpdateTime(updateTime)
                        .buildUpdateStatus(null)
                        .buildImgUrl((String) map.get("imgUrl"))
                        .buildChapterInfoList(startElements(html, "div.read_list a"))
                        .build();
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = node.href("a");
                try {
                    title = title.split(" ", 2)[1];
                } catch (Exception ignored) {
                }
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        JsoupNode node = new JsoupNode(html);
        node.init(node.html("div#view_content_txt"));
        node.remove("div");
        String content = SourceHelper.getCommonContent(node.clean());
        return SourceHelper.getContentList(new Content(chapterId, content));
    }

    @Override
    public Map<String, String> getRankMap() {
        Map<String, String> map = new LinkedHashMap<>();
        String html = "<ul id=\"globalNavUL\">\n" +
                "                        <li><a href=\"http://www.iddwx.com/soft1/\" title=\"玄幻奇幻\">玄幻奇幻</a></li><li><a href=\"http://www.iddwx.com/soft2/\" title=\"仙侠修真\">仙侠修真</a></li><li><a href=\"http://www.iddwx.com/soft3/\" title=\"穿越言情\">穿越言情</a></li><li><a href=\"http://www.iddwx.com/soft4/\" title=\"都市官场\">都市官场</a></li><li><a href=\"http://www.iddwx.com/soft5/\" title=\"历史架空\">历史架空</a></li><li><a href=\"http://www.iddwx.com/soft6/\" title=\"网游同人\">网游同人</a></li><li><a href=\"http://www.iddwx.com/soft7/\" title=\"科幻战争\">科幻战争</a></li><li><a href=\"http://www.iddwx.com/soft8/\" title=\"名著其他\">名著其他</a></li>\n" +
                "        </ul>";
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), node.href("a"));
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.title("a"))
                        .buildAuthor(null)
                        .buildUpdateTime(null)
                        .buildUpdateChapter(null)
                        .buildImgUrl(null)
                        .buildDetailUrl(node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "ul#mainlistUL div.mainListInfo");
    }
}
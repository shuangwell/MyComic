package top.luqichuang.common.source.comic;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2023/1/27 14:01
 * @ver 1.0
 */
public class Lai extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.LAI;
    }

    @Override
    public String getIndex() {
        return "https://www.laimanhua8.com";
    }

    @Override
    public String getCharsetName(String tag) {
        return "GB2312";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/cse1/search/?key=%s", getIndex(), DecryptUtil.getGBKEncodeStr(searchString));
        return NetUtil.postRequest(url, "", "");
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("dt a"))
                        .buildAuthor(null)
                        .buildUpdateTime(node.text("dd span"))
                        .buildUpdateChapter(node.ownText("a.yellow"))
                        .buildImgUrl(node.src("img"))
                        .buildDetailUrl(getIndex() + node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "div#dmList li");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected void dealInfo(JsoupNode node) {
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("div.title h1"))
                        .buildAuthor(node.ownText("div.info p", 1))
                        .buildIntro(node.ownText("div#intro1 p"))
                        .buildUpdateTime(node.ownText("div.info p span"))
                        .buildUpdateStatus(null)
                        .buildImgUrl(node.src("div.info_cover img"))
                        .buildChapterInfoList(startElements(html, "div#play_0 li"))
                        .build();
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        List<String> urlList = new ArrayList<>();
        try {
            String server = "https://mhpic789-5.kingwar.cn";
            String picTree = StringUtil.match("var picTree ='(.*?)';", html);
            picTree = DecryptUtil.decryptBase64(picTree);
            String[] pics = picTree.split("\\$qingtiandy\\$");
            for (String pic : pics) {
                urlList.add(server + pic);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return SourceHelper.getContentList(urlList, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<ul class=\"nav_menu\"><li><a href=\"/kanmanhua/rexue/\"title=\"热血\">少年热血</a></li><li><a href=\"/kanmanhua/gedou/\"title=\"格斗\">武侠格斗</a></li><li><a href=\"/kanmanhua/kehuan/\"title=\"科幻\">科幻魔幻</a></li><li><a href=\"/kanmanhua/jingji/\"title=\"竞技\">竞技体育</a></li><li><a href=\"/kanmanhua/gaoxiao/\"title=\"搞笑\">爆笑喜剧</a></li><li><a href=\"/kanmanhua/tuili/\"title=\"推理\">侦探推理</a></li><li><a href=\"/kanmanhua/kongbu/\"title=\"恐怖\">恐怖灵异</a></li><li><a href=\"/kanmanhua/danmei/\"title=\"耽美\">耽美人生</a></li><li><a href=\"/kanmanhua/shaonv/\"title=\"少女爱情\">少女</a></li><li><a href=\"/kanmanhua/lianai/\"title=\"恋爱生活\">恋爱</a></li><li><a href=\"/kanmanhua/shenghuo/\"title=\"生活漫画\">生活</a></li></ul>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a") + "%d.html");
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }
}

package top.luqichuang.common.source.comic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.ChapterInfoBuilder;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.starter.ElementNode;
import top.luqichuang.common.starter.ElementStarter;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/6/1 11:50
 * @ver 1.0
 */
public class BaoZi extends BaseComicSource {

    /**
     * 设置对应枚举
     *
     * @return CSourceEnum
     */
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.BAO_ZI;
    }

    /**
     * 设置主页url
     *
     * @return String
     */
    @Override
    public String getIndex() {
        return "https://cn.baozimh.com";
    }

    /**
     * 根据搜索关键词拼凑出搜索url，返回request
     *
     * @param searchString searchString
     * @return Request
     */
    @Override
    public Request getSearchRequest(String searchString) {
        String url = getIndex() + "/search?q=" + searchString;
        return NetUtil.getRequest(url);
    }

    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (CONTENT.equals(tag)) {
            ElementNode node = new ElementNode(html);
            String nextUrl = node.href("div.comic-chapter>div.next_chapter a");
            String text = node.ownText("div.comic-chapter>div.next_chapter a");
            List<String> htmlList = (List<String>) map.get("htmlList");
            if (htmlList == null) {
                htmlList = new ArrayList<>();
                map.put("htmlList", htmlList);
            }
            htmlList.add(html);
            if (nextUrl != null && "点击进入下一页".equals(text)) {
                return NetUtil.getRequest(nextUrl);
            }
        }
        return super.buildRequest(html, tag, data, map);
    }

    protected InfoListConfig getInfoListConfig(ElementNode node) {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"div.comics-card"};
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(node.title("a"))
                                .buildAuthor(node.ownText("a.comics-card__info small"))
                                .buildUpdateTime(null)
                                .buildUpdateChapter(null)
                                .buildImgUrl(node.src("amp-img"))
                                .buildDetailUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    protected InfoDetailConfig getInfoDetailConfig(ElementNode node) {
        return new InfoDetailConfig() {
            @Override
            protected void setData(EntityInfo info, ElementNode node) {
                String updateTime = node.ownText("div.supporting-text em");
                updateTime = StringUtil.remove(updateTime, "(");
                updateTime = StringUtil.remove(updateTime, " 更新)");
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("h1.comics-detail__title"))
                        .buildAuthor(node.ownText("h2.comics-detail__author"))
                        .buildIntro(node.ownText("p.comics-detail__desc"))
                        .buildUpdateTime(updateTime)
                        .buildUpdateStatus(node.ownText("div.tag-list span"))
                        .buildImgUrl(node.src("div.de-info__box amp-img"))
                        .build();
            }

            @Override
            protected void addListConfig(List<ListConfig<ChapterInfo>> configList) {
                configList.add(new ListConfig<ChapterInfo>() {
                    @Override
                    protected boolean needSwap(List<ChapterInfo> list) {
                        return true;
                    }

                    @Override
                    protected String[] getQuery() {
                        return new String[]{
                                "div.pure-g[id] div.comics-chapters",
                                "div.l-box div.comics-chapters",
                        };
                    }

                    @Override
                    protected ChapterInfo getData(ElementNode node) {
                        return new ChapterInfoBuilder()
                                .buildTitle(node.ownText("span"))
                                .buildUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    protected ContentListConfig getContentListConfig(ElementNode node) {
        return new ContentListConfig() {
            @Override
            protected void addUrlList(ElementNode node, List<String> urlList, Map<String, Object> map) {
                List<String> htmlList = (List<String>) map.get("htmlList");
                if (htmlList != null) {
                    for (String html : htmlList) {
                        ElementStarter<String> starter = new ElementStarter<String>() {
                            @Override
                            protected String loadListData(ElementNode node) {
                                String server = "https://s2.baozimh.com/";
                                String url = node.src("amp-img");
                                if (url != null) {
                                    String s = StringUtil.match("(.*?)://(.*?)/(.+)", url, 3);
                                    if (s != null) {
                                        url = server + s;
                                    }
                                }
                                if (!urlList.contains(url)) {
                                    return url;
                                } else {
                                    return null;
                                }
                            }
                        };
                        urlList.addAll(starter.startList(html, "ul.comic-contain amp-img"));
                    }
                }
            }
        };
    }

    protected RankMapConfig getRankMapConfig() {
        return new RankMapConfig() {
            @Override
            protected String getHtml() {
                return "<div style=\"margin-top: 60px;\"data-v-f713d122=\"\"><div class=\"classify-nav\"data-v-f713d122=\"\"><div class=\"nav\"data-v-f713d122=\"\"><a href=\"/classify?type=all&amp;region=all&amp;state=all\"class=\"item active\"data-v-f713d122=\"\">全部</a><a href=\"/classify?type=all&amp;region=cn&amp;state=all\"class=\"item\"data-v-f713d122=\"\">国漫</a><a href=\"/classify?type=all&amp;region=jp&amp;state=all\"class=\"item\"data-v-f713d122=\"\">日本</a><a href=\"/classify?type=all&amp;region=kr&amp;state=all\"class=\"item\"data-v-f713d122=\"\">韩国</a><a href=\"/classify?type=all&amp;region=en&amp;state=all\"class=\"item\"data-v-f713d122=\"\">欧美</a></div></div><div class=\"classify-nav\"data-v-f713d122=\"\"><div class=\"nav pure-form\"data-v-f713d122=\"\"><a href=\"/classify?type=all&amp;region=all&amp;state=serial\"class=\"item\"data-v-f713d122=\"\">连载中</a><a href=\"/classify?type=all&amp;region=all&amp;state=pub\"class=\"item\"data-v-f713d122=\"\">已完结</a></div></div><div class=\"classify-nav\"data-v-f713d122=\"\"><div class=\"nav\"data-v-f713d122=\"\"><a href=\"/classify?type=lianai&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">恋爱</a><a href=\"/classify?type=chunai&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">纯爱</a><a href=\"/classify?type=gufeng&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">古风</a><a href=\"/classify?type=yineng&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">异能</a><a href=\"/classify?type=xuanyi&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">悬疑</a><a href=\"/classify?type=juqing&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">剧情</a><a href=\"/classify?type=kehuan&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">科幻</a><a href=\"/classify?type=qihuan&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">奇幻</a><a href=\"/classify?type=xuanhuan&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">玄幻</a><a href=\"/classify?type=chuanyue&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">穿越</a><a href=\"/classify?type=mouxian&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">冒险</a><a href=\"/classify?type=tuili&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">推理</a><a href=\"/classify?type=wuxia&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">武侠</a><a href=\"/classify?type=gedou&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">格斗</a><a href=\"/classify?type=zhanzheng&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">战争</a><a href=\"/classify?type=rexie&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">热血</a><a href=\"/classify?type=gaoxiao&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">搞笑</a><a href=\"/classify?type=danuzhu&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">大女主</a><a href=\"/classify?type=dushi&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">都市</a><a href=\"/classify?type=zongcai&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">总裁</a><a href=\"/classify?type=hougong&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">后宫</a><a href=\"/classify?type=richang&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">日常</a><a href=\"/classify?type=hanman&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">韩漫</a><a href=\"/classify?type=shaonian&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">少年</a><a href=\"/classify?type=qita&amp;region=all&amp;state=all\"class=\"item\"data-v-f713d122=\"\">其它</a></div></div><div class=\"classify-nav\"data-v-f713d122=\"\"><div class=\"nav\"data-v-f713d122=\"\"><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=ABCD\"class=\"item\"data-v-f713d122=\"\">ABCD</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=EFGH\"class=\"item\"data-v-f713d122=\"\">EFGH</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=IJKL\"class=\"item\"data-v-f713d122=\"\">IJKL</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=MNOP\"class=\"item\"data-v-f713d122=\"\">MNOP</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=QRST\"class=\"item\"data-v-f713d122=\"\">QRST</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=UVW\"class=\"item\"data-v-f713d122=\"\">UVW</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=XYZ\"class=\"item\"data-v-f713d122=\"\">XYZ</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=0-9\"class=\"item\"data-v-f713d122=\"\">0-9</a></div></div></div>";
            }

            @Override
            protected String getCssQuery() {
                return "a";
            }

            @Override
            protected String getTitle(ElementNode node) {
                return node.ownText("a");
            }

            @Override
            protected String getUrl(ElementNode node) {
                return getIndex() + node.href("a") + "&page=%d";
            }
        };
    }

//    /**
//     * 为应对某些多次访问才可以获取数据的网页
//     * 调用时间在获取request(getSearchRequest...)之前
//     *
//     * @param html 获取的网页源码
//     * @param tag  访问页面标识(Source.SEARCH DETAIL...)
//     * @param data 访问页面后存留的信息
//     * @param map  给后续调用保存的主要信息
//     * @return Request
//     */
//    @Override
//    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
//        if (CONTENT.equals(tag)) {
//            JsoupNode node = new JsoupNode(html);
//            String nextUrl = node.href("div.comic-chapter>div.next_chapter a");
//            String text = node.ownText("div.comic-chapter>div.next_chapter a");
//            try {
//                List<String> list;
//                if (map.get("list") != null) {
//                    list = new ArrayList<>((List<String>) map.get("list"));
//                } else {
//                    list = new ArrayList<>();
//                }
//                Elements elements = node.getElements("ul.comic-contain amp-img");
//                for (Element element : elements) {
//                    node.init(element);
//                    String url = node.src("amp-img");
//                    String server = "https://s2.baozimh.com/";
//                    if (url != null) {
//                        String s = StringUtil.match("(.*?)://(.*?)/(.+)", url, 3);
//                        if (s != null) {
//                            url = server + s;
//                        }
//                    }
//                    if (!list.contains(url)) {
//                        list.add(url);
//                    }
//                }
//                map.put("list", list);
//                if (nextUrl != null && "点击进入下一页".equals(text)) {
//                    return NetUtil.getRequest(nextUrl);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return super.buildRequest(html, tag, data, map);
//    }
//
//    /**
//     * 返回搜索页面的所有漫画链表
//     *
//     * @param html html
//     * @return List<EntityInfo>
//     */
//    @Override
//    public List<EntityInfo> getInfoList(String html) {
//        //重写JsoupStarter的dealElement方法
//        //设置漫画的名称、作者等信息
//        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
//            @Override
//            protected EntityInfo dealElement(JsoupNode node) {
//                return new EntityInfoBuilder(getInfoClass())
//                        .buildSourceId(getSourceId())
//                        .buildTitle(node.title("a"))
//                        .buildAuthor(node.ownText("a.comics-card__info small"))
//                        .buildUpdateTime(null)
//                        .buildUpdateChapter(null)
//                        .buildImgUrl(node.src("amp-img"))
//                        .buildDetailUrl(getIndex() + node.href("a"))
//                        .build();
//            }
//        };
//        //startElements方法 返回漫画链表
//        return starter.startElements(html, "div.comics-card");
//    }
//
//    @Override
//    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
//        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
//            //重写isDESC方法 设置章节顺序
//            @Override
//            protected boolean isDESC() {
//                return false;
//            }
//
//            //设置漫画基本信息
//            @Override
//            protected void dealInfo(JsoupNode node) {
//                new EntityInfoBuilder(info)
//                        .buildSourceId(getSourceId())
//                        .buildTitle(node.ownText("h1.comics-detail__title"))
//                        .buildAuthor(node.ownText("h2.comics-detail__author"))
//                        .buildIntro(node.ownText("p.comics-detail__desc"))
//                        .buildUpdateTime(node.ownText("div.supporting-text em"))
//                        .buildUpdateStatus(node.ownText("div.tag-list span"))
//                        .buildImgUrl(node.src("div.de-info__box amp-img"))
//                        .buildChapterInfoList(startElements(html, "div.pure-g[id] div.comics-chapters"))
//                        .buildChapterInfoList(startElements(html, "div.l-box div.comics-chapters"))
//                        .build();
//            }
//
//            //设置漫画章节信息
//            @Override
//            protected ChapterInfo dealElement(JsoupNode node) {
//                String title = node.ownText("span");
//                String chapterUrl = getIndex() + node.href("a");
//                return new ChapterInfo(title, chapterUrl);
//            }
//        };
//        //使dealInfo方法生效
//        starter.startInfo(html);
//    }
//
//    /**
//     * 根据获取的内容页html解析出对应的图片url
//     * 不同网页一般解析方式不同
//     *
//     * @param html      html
//     * @param chapterId 章节ID
//     * @param map       buildRequest传入的漫画信息
//     * @return List<Content>
//     */
//    @Override
//    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
//        List<String> list = null;
//        try {
//            list = (List<String>) map.get("list");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return SourceHelper.getContentList(list, chapterId);
//    }
//
//    /**
//     * 设置排行榜的 名称--url map
//     *
//     * @return Map<String, String>
//     */
//    @Override
//    public Map<String, String> getRankMap() {
//        //截取网页一段html 解析出对应map
//        String html = "<div style=\"margin-top: 60px;\"data-v-f713d122=\"\"><div class=\"classify-nav\"data-v-f713d122=\"\"><div class=\"nav\"data-v-f713d122=\"\"><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp\"class=\"item active\"data-v-f713d122=\"\">全部</a><a href=\"/classify?type=all&amp;region=cn&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">国漫</a><a href=\"/classify?type=all&amp;region=jp&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">日本</a><a href=\"/classify?type=all&amp;region=kr&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">韩国</a><a href=\"/classify?type=all&amp;region=en&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">欧美</a></div></div><div class=\"classify-nav\"data-v-f713d122=\"\"><div class=\"nav pure-form\"data-v-f713d122=\"\"><a href=\"/classify?type=all&amp;region=all&amp;state=serial&amp\"class=\"item\"data-v-f713d122=\"\">连载中</a><a href=\"/classify?type=all&amp;region=all&amp;state=pub&amp\"class=\"item\"data-v-f713d122=\"\">已完结</a></div></div><div class=\"classify-nav\"data-v-f713d122=\"\"><div class=\"nav\"data-v-f713d122=\"\"><a href=\"/classify?type=lianai&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">恋爱</a><a href=\"/classify?type=chunai&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">纯爱</a><a href=\"/classify?type=gufeng&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">古风</a><a href=\"/classify?type=yineng&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">异能</a><a href=\"/classify?type=xuanyi&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">悬疑</a><a href=\"/classify?type=juqing&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">剧情</a><a href=\"/classify?type=kehuan&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">科幻</a><a href=\"/classify?type=qihuan&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">奇幻</a><a href=\"/classify?type=xuanhuan&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">玄幻</a><a href=\"/classify?type=chuanyue&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">穿越</a><a href=\"/classify?type=mouxian&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">冒险</a><a href=\"/classify?type=tuili&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">推理</a><a href=\"/classify?type=wuxia&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">武侠</a><a href=\"/classify?type=gedou&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">格斗</a><a href=\"/classify?type=zhanzheng&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">战争</a><a href=\"/classify?type=rexie&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">热血</a><a href=\"/classify?type=gaoxiao&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">搞笑</a><a href=\"/classify?type=danuzhu&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">大女主</a><a href=\"/classify?type=dushi&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">都市</a><a href=\"/classify?type=zongcai&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">总裁</a><a href=\"/classify?type=hougong&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">后宫</a><a href=\"/classify?type=richang&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">日常</a><a href=\"/classify?type=hanman&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">韩漫</a><a href=\"/classify?type=shaonian&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">少年</a><a href=\"/classify?type=qita&amp;region=all&amp;state=all&amp\"class=\"item\"data-v-f713d122=\"\">其它</a></div></div><div class=\"classify-nav\"data-v-f713d122=\"\"><div class=\"nav\"data-v-f713d122=\"\"><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=ABCD\"class=\"item\"data-v-f713d122=\"\">ABCD</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=EFGH\"class=\"item\"data-v-f713d122=\"\">EFGH</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=IJKL\"class=\"item\"data-v-f713d122=\"\">IJKL</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=MNOP\"class=\"item\"data-v-f713d122=\"\">MNOP</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=QRST\"class=\"item\"data-v-f713d122=\"\">QRST</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=UVW\"class=\"item\"data-v-f713d122=\"\">UVW</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=XYZ\"class=\"item\"data-v-f713d122=\"\">XYZ</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=0-9\"class=\"item\"data-v-f713d122=\"\">0-9</a></div></div></div>";
//        Map<String, String> map = new LinkedHashMap<>();
//        JsoupNode node = new JsoupNode(html);
//        Elements elements = node.getElements("a");
//        for (Element element : elements) {
//            node.init(element);
//            map.put(node.ownText("a"), getIndex() + node.href("a") + "&page=%d");
//        }
//        return map;
//    }
//
//    /**
//     * 返回排行榜的漫画链表
//     *
//     * @param html html
//     * @return List<EntityInfo>
//     */
//    @Override
//    public List<EntityInfo> getRankInfoList(String html) {
//        return getInfoList(html);
//    }
}

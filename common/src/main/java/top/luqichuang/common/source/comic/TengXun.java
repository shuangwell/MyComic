package top.luqichuang.common.source.comic;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2020/8/12 15:25
 * @ver 1.0
 */
public class TengXun extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.TENG_XUN;
    }

    @Override
    public String getIndex() {
        return "https://ac.qq.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        searchString = "https://ac.qq.com/Comic/searchList?search=" + searchString;
        return NetUtil.getRequest(searchString);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.title("a"))
                        .buildAuthor(null)
                        .buildUpdateTime(null)
                        .buildUpdateChapter(node.ownText("h3"))
                        .buildImgUrl(node.attr("img", "data-original"))
                        .buildDetailUrl(getIndex() + node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "ul.mod_book_list li");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {

            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("h2.works-intro-title.ui-left strong"))
                        .buildAuthor(node.ownText("div.works-intro span.first em"))
                        .buildIntro(node.ownText("div.works-intro p.works-intro-short"))
                        .buildUpdateTime(node.ownText("ul.ui-left span.ui-pl10"))
                        .buildUpdateStatus(node.ownText("div.works-intro label.works-intro-status"))
                        .buildImgUrl(node.src("div.works-cover.ui-left img"))
                        .buildChapterInfoList(startElements(html, "ol.chapter-page-all span.works-chapter-item"))
                        .build();
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String raw = StringUtil.match("DATA.*=.*'(.*?)',", html);
        String nonce = StringUtil.matchLast("window\\[.*?\\] *=(.*?);", html);
        if (nonce != null) {
            String[] docs = nonce.split("\\(\\)");
            for (String doc : docs) {
                String tmp = StringUtil.match("(\\(.*document.*\\)\\.toString)", doc);
                if (tmp == null) {
                    tmp = StringUtil.match("(\\(.*window.*\\)\\.toString)", doc);
                }
                if (tmp != null) {
                    nonce = nonce.replace(tmp + "()", "0");
                }
            }
            nonce = DecryptUtil.exeJsCode(nonce);
        }
        String data = DecryptUtil.exeJsFunction(getJsCode(), "decode", raw, nonce);
        List<String> urlList = null;
        if (data != null) {
            data = DecryptUtil.decryptBase64(data);
            if (data != null) {
                data = data.replaceAll("\\\\", "");
                urlList = StringUtil.matchList("pid(.*?)\"url\":\"(.*?)\"", data, 2);
            }
        }
        return SourceHelper.getContentList(urlList, chapterId);
    }

    private String getJsCode() {
        return "function decode(T, N) {\n" +
                "\tvar len, locate, str;\n" +
                "\tT = T.split('');\n" +
                "\tN = N.match(/\\d+[a-zA-Z]+/g);\n" +
                "\tlen = N.length;\n" +
                "\twhile (len--) {\n" +
                "\t\tlocate = parseInt(N[len]) & 255;\n" +
                "\t\tstr = N[len].replace(/\\d+/g, '');\n" +
                "\t\tT.splice(locate, str.length)\n" +
                "\t}\n" +
                "\tT = T.join('');\n" +
                "\treturn T;\n" +
                "}";
    }

    @Override
    public Map<String, String> getRankMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("飙升榜", "https://m.ac.qq.com/rank/index?type=rise&page=%d");
        map.put("畅销榜", "https://m.ac.qq.com/rank/index?type=pay&page=%d");
        map.put("新作榜", "https://m.ac.qq.com/rank/index?type=new&page=%d");
        map.put("真香榜", "https://m.ac.qq.com/rank/index?type=hot&page=%d");
        String html = "<a href=\"javascript:void(0)\"title=\"付费\"id=\"vip/2\">付费</a><a href=\"javascript:void(0)\"title=\"免费\"id=\"vip/1\">免费</a><a href=\"javascript:void(0)\"title=\"连载\"id=\"finish/1\">连载</a><a href=\"javascript:void(0)\"title=\"完结\"id=\"finish/2\">完结</a><a href=\"javascript:void(0)\"title=\"恋爱\"id=\"theme/105\">恋爱</a><a href=\"javascript:void(0)\"title=\"玄幻\"id=\"theme/101\">玄幻</a><a href=\"javascript:void(0)\"title=\"异能\"id=\"theme/103\">异能</a><a href=\"javascript:void(0)\"title=\"恐怖\"id=\"theme/110\">恐怖</a><a href=\"javascript:void(0)\"title=\"剧情\"id=\"theme/106\">剧情</a><a href=\"javascript:void(0)\"title=\"科幻\"id=\"theme/108\">科幻</a><a href=\"javascript:void(0)\"title=\"悬疑\"id=\"theme/112\">悬疑</a><a href=\"javascript:void(0)\"title=\"奇幻\"id=\"theme/102\">奇幻</a><a href=\"javascript:void(0)\"title=\"冒险\"id=\"theme/104\">冒险</a><a href=\"javascript:void(0)\"title=\"犯罪\"id=\"theme/111\">犯罪</a><a href=\"javascript:void(0)\"title=\"动作\"id=\"theme/109\">动作</a><a href=\"javascript:void(0)\"title=\"日常\"id=\"theme/113\">日常</a><a href=\"javascript:void(0)\"title=\"竞技\"id=\"theme/114\">竞技</a><a href=\"javascript:void(0)\"title=\"武侠\"id=\"theme/115\">武侠</a><a href=\"javascript:void(0)\"title=\"历史\"id=\"theme/116\">历史</a><a href=\"javascript:void(0)\"title=\"战争\"id=\"theme/117\">战争</a>";
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            String url = String.format("%s/Comic/all/%s/search/hot", getIndex(), node.attr("a", "id")) + "/page/%d";
            map.put(node.ownText("a"), url);
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        List<EntityInfo> list = new ArrayList<>();
        JsoupStarter<EntityInfo> starter1 = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                String detailUrl = getIndex() + node.href("a");
                detailUrl = StringUtil.replace(detailUrl, "comic/index", "Comic/ComicInfo");
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("strong.comic-title"))
                        .buildAuthor(null)
                        .buildUpdateTime(null)
                        .buildUpdateChapter(node.ownText("small.comic-update"))
                        .buildImgUrl(node.src("img"))
                        .buildDetailUrl(detailUrl)
                        .build();
            }
        };
        JsoupStarter<EntityInfo> starter2 = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                String detailUrl = getIndex() + node.href("a");
                detailUrl = StringUtil.replace(detailUrl, "comic/index", "Comic/ComicInfo");
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.title("h3.ret-works-title a"))
                        .buildAuthor(node.title("p.ret-works-author"))
                        .buildUpdateTime(null)
                        .buildUpdateChapter(null)
                        .buildImgUrl(node.attr("img", "data-original"))
                        .buildDetailUrl(detailUrl)
                        .build();
            }
        };
        list.addAll(starter1.startElements(html, "div.top3-box-item1"));
        list.addAll(starter1.startElements(html, "div.top3-box-item2"));
        list.addAll(starter1.startElements(html, "div.top3-box-item3"));
        list.addAll(starter1.startElements(html, "li.comic-item"));
        list.addAll(starter2.startElements(html, "ul.clearfix li"));
        return list;
    }
}

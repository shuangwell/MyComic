package top.luqichuang.common.source.comic;

import com.alibaba.fastjson.JSONArray;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.json.JsonNode;
import top.luqichuang.common.json.JsonStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2020/8/12 15:25
 * @ver 1.0
 */
public class BiliBili extends BaseComicSource {
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.BILI_BILI;
    }

    @Override
    public String getIndex() {
        return "https://manga.bilibili.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = "https://manga.bilibili.com/twirp/comic.v1.Comic/Search?device=pc&platform=web";
        Map<String, String> map = new HashMap<>();
        map.put("key_word", searchString);
        map.put("page_num", "1");
        map.put("page_size", "9");
        return NetUtil.postRequest(url, map);
    }

    @Override
    public Request getDetailRequest(String detailUrl) {
        String url = "https://manga.bilibili.com/twirp/comic.v1.Comic/ComicDetail?device=pc&platform=web";
        String id = StringUtil.match("(\\d+)", detailUrl);
        return NetUtil.postRequest(url, "comic_id", id);
    }

    @Override
    public Request getContentRequest(String imageUrl) {
        String url = "https://manga.bilibili.com/twirp/comic.v1.Comic/GetImageIndex?device=pc&platform=web";
        String id = StringUtil.matchLast("(\\d+)", imageUrl);
        return NetUtil.postRequest(url, "ep_id", id);
    }

    @Override
    public Request getRankRequest(String rankUrl) {
        String[] attrs = rankUrl.split("#");
        String url = String.format("https://manga.bilibili.com/twirp/comic.v1.Comic/%s?device=pc&platform=web", attrs[0]);
        JsonNode node = new JsonNode(attrs[1]);
        Map<String, String> map = new HashMap<>();
        for (String key : node.keySet()) {
            map.put(key, node.string(key));
        }
        return NetUtil.postRequest(url, map);
    }

    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (DETAIL.equals(tag) && map.isEmpty()) {
            map.put("url", data.get("url"));
        } else if (CONTENT.equals(tag) && map.isEmpty()) {
            JsonStarter<Object> starter = new JsonStarter<Object>() {
                @Override
                public Object dealDataList(JsonNode node) {
                    return node.string("path");
                }
            };
            List<Object> list = starter.startDataList(html, "data", "images");
            com.alibaba.fastjson.JSONArray array = new JSONArray(list);
            String url = "https://manga.bilibili.com/twirp/comic.v1.Comic/ImageToken?device=pc&platform=web";
            map.put("url", url);
            return NetUtil.postRequest(url, "urls", array.toString());
        }
        return super.buildRequest(html, tag, data, map);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsonStarter<EntityInfo> starter = new JsonStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealDataList(JsonNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.string("org_title"))
                        .buildAuthor(node.arrayToString("author_name"))
                        .buildUpdateTime(null)
                        .buildUpdateChapter(null)
                        .buildImgUrl(node.string("vertical_cover"))
                        .buildDetailUrl("https://manga.bilibili.com/detail/mc" + node.string("id"))
                        .build();
            }
        };
        return starter.startDataList(html, "data", "list");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        String detailUrl = (String) map.get("url");
        String id = StringUtil.match("(\\d+)", detailUrl);
        JsonStarter<ChapterInfo> starter = new JsonStarter<ChapterInfo>() {
            @Override
            protected void dealData(JsonNode node) {
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.string("title"))
                        .buildAuthor(node.arrayToString("author_name"))
                        .buildIntro(node.string("classic_lines"))
                        .buildUpdateTime(null)
                        .buildUpdateStatus(node.string("renewal_time"))
                        .buildImgUrl(node.string("square_cover"))
                        .buildChapterInfoList(startDataList(html, "data", "ep_list"))
                        .build();
            }

            @Override
            protected ChapterInfo dealDataList(JsonNode node) {
                if (info.getUpdateTime() == null) {
                    info.setUpdateTime(node.string("index_last_modified"));
                }
                String title = node.string("short_title");
                String oTitle = node.string("title");
                if (oTitle != null) {
                    title = title + " " + oTitle;
                }
                String chapterUrl = "https://manga.bilibili.com/mc" + id + "/" + node.string("id");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startData(html, "data");
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        JsonStarter<String> starter = new JsonStarter<String>() {
            @Override
            protected String dealDataList(JsonNode node) {
                String url = node.string("url");
                String token = node.string("token");
                String chapterUrl = url + "?token=" + token;
                chapterUrl = chapterUrl.replace("\\u0026", "&");
                return chapterUrl;
            }
        };
        List<String> urlList = starter.startDataList(html, "data");
        return SourceHelper.getContentList(urlList, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        Map<String, String> map = new LinkedHashMap<>();
        //https://manga.bilibili.com/twirp/comic.v1.Comic/HomeHot?device=pc&platform=web {"type":3}
        map.put("日漫榜", "HomeHot#{\"type\":3}");
        map.put("国漫榜", "HomeHot#{\"type\":4}");//{"type":4}
        map.put("月票榜", "HomeFans#{\"last_week_offset\":0,\"last_month_offset\":0,\"type\":1}");//{"last_week_offset":0,"last_month_offset":0,"type":1}
        map.put("投喂榜", "HomeFans#{\"last_week_offset\":0,\"last_month_offset\":0,\"type\":0}");//{"last_week_offset":0,"last_month_offset":0,"type":0}
        map.put("飙升榜", "HomeHot#{\"type\":2}");//{"type":2}
        map.put("免费榜", "HomeHot#{\"type\":1}");//{"type":1}
        //https://manga.bilibili.com/twirp/comic.v1.Comic/ClassPage?device=pc&platform=web {"style_id":-1,"area_id":1,"is_finish":-1,"order":0,"page_num":1,"page_size":200,"is_free":-1}
        map.put("免费", "ClassPage#{\"style_id\":-1,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":1}");
        map.put("付费", "ClassPage#{\"style_id\":-1,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":2}");
        map.put("等就免费", "ClassPage#{\"style_id\":-1,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":3}");
        map.put("大陆", "ClassPage#{\"style_id\":-1,\"area_id\":1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("日本", "ClassPage#{\"style_id\":-1,\"area_id\":2,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("韩国", "ClassPage#{\"style_id\":-1,\"area_id\":6,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("连载", "ClassPage#{\"style_id\":-1,\"area_id\":-1,\"is_finish\":0,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("完结", "ClassPage#{\"style_id\":-1,\"area_id\":-1,\"is_finish\":1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("正能量", "ClassPage#{\"style_id\":1028,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("冒险", "ClassPage#{\"style_id\":1013,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("热血", "ClassPage#{\"style_id\":999,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("搞笑", "ClassPage#{\"style_id\":994,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("恋爱", "ClassPage#{\"style_id\":995,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("少女", "ClassPage#{\"style_id\":1026,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("日常", "ClassPage#{\"style_id\":1020,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("校园", "ClassPage#{\"style_id\":1001,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("运动", "ClassPage#{\"style_id\":1010,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("治愈", "ClassPage#{\"style_id\":1007,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("古风", "ClassPage#{\"style_id\":997,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("玄幻", "ClassPage#{\"style_id\":1016,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("奇幻", "ClassPage#{\"style_id\":998,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("后宫", "ClassPage#{\"style_id\":1017,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("惊奇", "ClassPage#{\"style_id\":996,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("悬疑", "ClassPage#{\"style_id\":1023,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("都市", "ClassPage#{\"style_id\":1002,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("总裁", "ClassPage#{\"style_id\":1004,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        map.put("剧情", "ClassPage#{\"style_id\":1030,\"area_id\":-1,\"is_finish\":-1,\"order\":0,\"page_num\":%d,\"page_size\":20,\"is_free\":-1}");
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        JsonStarter<EntityInfo> starter = new JsonStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealDataList(JsonNode node) {
                String id = node.string("comic_id");
                if (id == null) {
                    id = node.string("season_id");
                }
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.string("title"))
                        .buildAuthor(node.arrayToString("author"))
                        .buildUpdateTime(null)
                        .buildUpdateChapter(node.string("last_short_title"))
                        .buildImgUrl(node.string("vertical_cover"))
                        .buildDetailUrl("https://manga.bilibili.com/detail/mc" + id)
                        .build();
            }
        };
        List<EntityInfo> list = starter.startDataList(html, "data");
        if (!list.isEmpty()) {
            return list;
        } else {
            return starter.startDataList(html, "data", "comics");
        }
    }
}

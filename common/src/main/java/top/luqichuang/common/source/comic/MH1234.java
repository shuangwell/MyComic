package top.luqichuang.common.source.comic;

import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.ChapterInfoBuilder;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.starter.ElementNode;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/7/28 13:27
 * @ver 1.0
 */
public class MH1234 extends BaseComicSource {
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.MH_1234;
    }

    @Override
    public String getIndex() {
        return "http://www.gmh1234.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format(getIndex() + "/search/?keywords=%s", searchString);
        return NetUtil.getRequest(url);
    }

    protected InfoListConfig getInfoListConfig(ElementNode node) {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"div#dmList li"};
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        String updateChapter = node.ownText("span.tt");
                        updateChapter = StringUtil.remove(updateChapter, "更新至");
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(node.ownText("dl a"))
                                .buildAuthor(null)
                                .buildUpdateTime(node.ownText("dd font"))
                                .buildUpdateChapter(updateChapter)
                                .buildImgUrl(node.src("a.cover img"))
                                .buildDetailUrl(node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    @Override
    protected InfoDetailConfig getInfoDetailConfig(ElementNode node) {
        return new InfoDetailConfig() {
            @Override
            protected void setData(EntityInfo info, ElementNode node) {
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("div.title h1"))
                        .buildAuthor(node.ownText("div.info p", 1))
                        .buildIntro(node.ownText("div#intro1 p"))
                        .buildUpdateTime(node.ownText("div.info span"))
                        .buildUpdateStatus(null)
                        .buildImgUrl(node.src("img.pic"))
                        .build();
            }

            @Override
            protected void addListConfig(List<ListConfig<ChapterInfo>> configList) {
                configList.add(new ListConfig<ChapterInfo>() {
                    @Override
                    protected boolean needSwap(List<ChapterInfo> list) {
                        return true;
                    }

                    @Override
                    protected String[] getQuery() {
                        return new String[]{"ul#chapter-list-1 li"};
                    }

                    @Override
                    protected ChapterInfo getData(ElementNode node) {
                        return new ChapterInfoBuilder()
                                .buildTitle(node.ownText("a"))
                                .buildUrl(getIndex() + node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    @Override
    protected ContentListConfig getContentListConfig(ElementNode node) {
        return new ContentListConfig() {
            @Override
            protected void addUrlList(ElementNode node, List<String> urlList, Map<String, Object> map) {
                String html = node.getHtml();
                String server = "https://gmh1234.wszwhg.net/";
                String chapterPath = StringUtil.match("chapterPath = \"(.*?)\"", html);
                String chapterImages = StringUtil.match("chapterImages = \\[(.*?)]", html);
                chapterImages = StringUtil.remove(chapterImages, "\"");
                String[] urls = StringUtil.split(chapterImages, ",");
                for (String url : urls) {
                    if (url.startsWith("http")) {
                        url = StringUtil.replace(url, "\\u0026", "&");
                        url = StringUtil.remove(url, "\\");
                    } else {
                        url = server + chapterPath + url;
                    }
                    urlList.add(url);
                }
            }
        };
    }

    @Override
    protected RankMapConfig getRankMapConfig() {
        return new RankMapConfig() {
            @Override
            protected String getHtml() {
                return "<ul class=\"nav_menu\">\t<li><a href=\"/comic/list/1/\" title=\"热血\">少年热血</a></li>\t<li><a href=\"/comic/list/2/\" title=\"格斗\">武侠格斗</a></li>\t<li><a href=\"/comic/list/3/\" title=\"科幻\">科幻魔幻</a></li>\t<li><a href=\"/comic/list/6/\" title=\"推理\">侦探推理</a></li>\t<li><a href=\"/comic/list/7/\" title=\"恐怖\">恐怖灵异</a></li>\t<li><a href=\"/comic/list/8/\" title=\"耽美\">耽美人生</a></li>\t<li><a href=\"/comic/list/9/\" title=\"少女\">少女爱情</a></li>\t<li><a href=\"/comic/list/10/\" title=\"恋爱\">恋爱生活</a></li>\t<li><a href=\"/comic/list/11/\" title=\"生活\">生活漫画</a></li>\t<li><a href=\"/comic/list/12/\" title=\"战争\">战争漫画</a></li></ul><div class=\"NfcharNav\">\t<a href=\"/comic/l/a.html\">A</a>\t<a href=\"/comic/l/b.html\">B</a>\t<a href=\"/comic/l/c.html\">C</a>\t<a href=\"/comic/l/d.html\">D</a>\t<a href=\"/comic/l/e.html\">E</a>\t<a href=\"/comic/l/f.html\">F</a>\t<a href=\"/comic/l/g.html\">G</a>\t<a href=\"/comic/l/h.html\">H</a>\t<a href=\"/comic/l/i.html\">I</a>\t<a href=\"/comic/l/j.html\">J</a>\t<a href=\"/comic/l/k.html\">K</a>\t<a href=\"/comic/l/l.html\">L</a>\t<a href=\"/comic/l/m.html\">M</a>\t<a href=\"/comic/l/n.html\">N</a>\t<a href=\"/comic/l/o.html\">O</a>\t<a href=\"/comic/l/p.html\">P</a>\t<a href=\"/comic/l/q.html\">Q</a>\t<a href=\"/comic/l/r.html\">R</a>\t<a href=\"/comic/l/s.html\">S</a>\t<a href=\"/comic/l/t.html\">T</a>\t<a href=\"/comic/l/u.html\">U</a>\t<a href=\"/comic/l/v.html\">V</a>\t<a href=\"/comic/l/w.html\">W</a>\t<a href=\"/comic/l/x.html\">X</a>\t<a href=\"/comic/l/y.html\">Y</a>\t<a href=\"/comic/l/z.html\">Z</a></div>";
            }

            @Override
            protected String getCssQuery() {
                return "a";
            }

            @Override
            protected String getTitle(ElementNode node) {
                return node.ownText("a");
            }

            @Override
            protected String getUrl(ElementNode node) {
                String url = getIndex() + node.href("a");
                url = StringUtil.replace(url, ".html", "/");
                return url + "%d.html";
            }
        };
    }
}

package top.luqichuang.common.source.comic;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.model.comic.BaseComicSource;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/11/19 13:44
 * @ver 1.0
 */
@Deprecated
public class PinYue extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.PIN_YUE;
    }

    @Override
    public String getIndex() {
        return "https://www.pinmh.com";
    }

    @Override
    public boolean isValid() {
        return false;//error index
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search/?keywords=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    public List<EntityInfo> getInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.title("a"))
                        .buildAuthor(node.ownText("p.auth"))
                        .buildUpdateTime(null)
                        .buildUpdateChapter(node.ownText("p.newPage"))
                        .buildImgUrl(node.src("img"))
                        .buildDetailUrl(node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "li.list-comic");
    }

    @Override
    public void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String author = node.ownText("ul.comic_deCon_liO li");
                author = StringUtil.remove(author, "作者：");
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(node.ownText("div.comic_deCon h1"))
                        .buildAuthor(author)
                        .buildIntro(node.ownText("p.comic_deCon_d"))
                        .buildUpdateTime(null)
                        .buildUpdateStatus(node.ownText("ul.comic_deCon_liO a"))
                        .buildImgUrl(node.src("div.comic_i_img img"))
                        .buildChapterInfoList(startElements(html, "ul#chapter-list-1 li"))
                        .build();
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("span.list_con_zj");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String[] urls = null;
        String chapterImages = StringUtil.match("chapterImages = \\[(.*?)\\];", html);
        if (chapterImages != null) {
            chapterImages = chapterImages.replace("\"", "").replace("\\", "");
            urls = chapterImages.split(",");
        }
        return SourceHelper.getContentList(urls, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<div class=\"wrap_ph_l con_left\"><ul class=\"ph_l_li\"><li class=\"active\"><a href=\"/rank/popularity/\">人气榜</a></li><li class=\"\"><a href=\"/rank/click/\">点击榜</a></li><li class=\"\"><a href=\"/rank/subscribe/\">订阅榜</a></li><li class=\"\"><a href=\"/rank/comment/\">评论榜</a></li><li class=\"\"><a href=\"/rank/criticism/\">吐槽榜</a></li></ul><ul class=\"ph_l_li\"><li class=\"\"><a href=\"/rank/mofa/\">魔法漫画</a></li><li class=\"\"><a href=\"/rank/shaonian/\">少年漫画</a></li><li class=\"\"><a href=\"/rank/shaonv/\">少女漫画</a></li><li class=\"\"><a href=\"/rank/qingnian/\">青年漫画</a></li><li class=\"\"><a href=\"/rank/gaoxiao/\">搞笑漫画</a></li><li class=\"\"><a href=\"/rank/kehuan/\">科幻漫画</a></li><li class=\"\"><a href=\"/rank/rexue/\">热血漫画</a></li><li class=\"\"><a href=\"/rank/maoxian/\">冒险漫画</a></li><li class=\"\"><a href=\"/rank/wanjie/\">完结漫画</a></li></ul></div>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<EntityInfo> getRankInfoList(String html) {
        JsoupStarter<EntityInfo> starter = new JsoupStarter<EntityInfo>() {
            @Override
            protected EntityInfo dealElement(JsoupNode node) {
                return new EntityInfoBuilder(getInfoClass())
                        .buildSourceId(getSourceId())
                        .buildTitle(node.title("div.con_li_content"))
                        .buildAuthor(node.ownText("li"))
                        .buildUpdateTime(null)
                        .buildUpdateChapter(node.ownText("li:eq(5) a"))
                        .buildImgUrl(node.src("img"))
                        .buildDetailUrl(node.href("a"))
                        .build();
            }
        };
        return starter.startElements(html, "div.ph_r_con_li_c");
    }
}
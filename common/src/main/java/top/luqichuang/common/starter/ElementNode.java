package top.luqichuang.common.starter;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.Set;

/**
 * @author 18472
 * @desc
 * @date 2024/3/28 21:04
 * @ver 1.0
 */
public class ElementNode {

    private String html;

    private Element element;

    private Elements elements = new Elements();

    private JSONObject jsonObject;

    private JSONArray jsonArray = new JSONArray();

    public ElementNode() {
    }

    public ElementNode(String html) {
        init(html);
    }

    public ElementNode(Element element) {
        this.element = element;
    }

    public ElementNode(Elements elements) {
        this.elements = elements;
    }

    public ElementNode(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public ElementNode(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    @Override
    public String toString() {
        return "ElementNode{" +
                "element=" + element +
                ", elements=" + elements +
                ", jsonObject=" + jsonObject +
                ", jsonArray=" + jsonArray +
                '}';
    }

    public void init(String html) {
        this.html = html;
        this.element = parseToElement(html);
        this.jsonObject = parseToJson(html);
        this.jsonArray = parseToArray(html);
    }

    public void init(Element element) {
        this.element = element;
    }

    public void init(Elements elements) {
        this.elements = elements;
    }

    public void init(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public void init(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    public String getHtml() {
        return html;
    }

    public Element parseToElement(String html) {
        try {
            return Jsoup.parse(html);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject parseToJson(String html) {
        try {
            return JSONObject.parseObject(html);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONArray parseToArray(String html) {
        try {
            return JSONArray.parseArray(html);
        } catch (Exception e) {
            return new JSONArray();
        }
    }

    /*============================================================================================*/
    public void initConditions(String... conditions) {
        try {
            for (String condition : conditions) {
                jsonObject = getJSONObject(condition);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Set<String> keySet() {
        try {
            return jsonObject.keySet();
        } catch (Exception e) {
            return new HashSet<>();
        }
    }

    public String arrayToString(String key) {
        try {
            String string = jsonObject.getJSONArray(key).toString();
            string = string.replace("[", "").replace("]", "").replace("\"", "");
            return string;
        } catch (Exception e) {
            return null;
        }
    }

    public String getString(String key) {
        try {
            return jsonObject.getString(key);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public JSONObject getJSONObject(String key) {
        try {
            return jsonObject.getJSONObject(key);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public JSONArray getJSONArray(String key) {
        try {
            return jsonObject.getJSONArray(key);
        } catch (Exception e) {
            return new JSONArray();
        }
    }

    /*============================================================================================*/
    public Element getElement() {
        return element;
    }

    public Element getElement(String cssQuery) {
        try {
            return element.selectFirst(cssQuery);
        } catch (Exception e) {
            return null;
        }
    }

    public Elements getElements() {
        return elements;
    }

    public Elements getElements(String cssQuery) {
        try {
            return element.select(cssQuery);
        } catch (Exception e) {
            return new Elements();
        }
    }

    public void addElement(String cssQuery) {
        addElement(getElement(cssQuery));
    }

    public void addElement(Element element) {
        if (element != null) {
            elements.add(element);
        }
    }

    public ElementNode select(String cssQuery) {
        try {
            return new ElementNode(element.selectFirst(cssQuery));
        } catch (Exception e) {
            return this;
        }
    }

    public ElementNode select(String cssQuery, int index) {
        try {
            return new ElementNode(element.select(cssQuery).get(index));
        } catch (Exception e) {
            return this;
        }
    }

    public String ownText(String cssQuery) {
        try {
            return element.selectFirst(cssQuery).ownText();
        } catch (Exception e) {
            return null;
        }
    }

    public String ownText(String cssQuery, int index) {
        try {
            return element.select(cssQuery).get(index).ownText();
        } catch (Exception e) {
            return null;
        }
    }

    public String html(String cssQuery) {
        try {
            return element.selectFirst(cssQuery).html();
        } catch (Exception e) {
            return null;
        }
    }

    public String text(String cssQuery) {
        try {
            return element.selectFirst(cssQuery).text();
        } catch (Exception e) {
            return null;
        }
    }

    public ElementNode remove(String... cssQuery) {
        try {
            for (String s : cssQuery) {
                element.select(s).remove();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public String clean() {
        return clean(element.html());
    }

    public String clean(String html) {
        return Jsoup.clean(html, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));
    }

    public String href(String cssQuery) {
        return attr(cssQuery, "href");
    }

    public String src(String cssQuery) {
        return attr(cssQuery, "src");
    }

    public String title(String cssQuery) {
        return attr(cssQuery, "title");
    }

    public String attr(String cssQuery, String attr) {
        try {
            return element.selectFirst(cssQuery).attr(attr).trim();
        } catch (Exception e) {
            return null;
        }
    }
}

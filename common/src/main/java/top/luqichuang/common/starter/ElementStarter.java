package top.luqichuang.common.starter;

import com.alibaba.fastjson.JSONObject;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import top.luqichuang.common.util.StringUtil;

/**
 * @author 18472
 * @desc
 * @date 2024/3/28 21:00
 * @ver 1.0
 */
public abstract class ElementStarter<T> {

    private ElementNode node = new ElementNode();

    protected boolean needSwap(List<T> list) {
        return false;
    }

    protected T loadData(ElementNode node) {
        return null;
    }

    protected T loadListData(ElementNode node) {
        return null;
    }

    public final T startData(String html) {
        node.init(html);
        return loadData(node);
    }

    public final List<T> startList(String html, String... query) {
        node.init(html);
        List<T> list = new ArrayList<>();
        if (node.getElement() != null) {
            for (String q : query) {
                Elements elements = node.getElements(q);
                for (Element element : elements) {
                    node.init(element);
                    T t = loadListData(node);
                    if (t != null) {
                        list.add(t);
                    }
                }
            }
        }
        if (node.getJsonObject() != null) {
            for (int i = 0; i < query.length; i++) {
                if (i != query.length - 1) {
                    node.init(node.getJSONObject(query[i]));
                } else {
                    node.init(node.getJSONArray(query[i]));
                }
            }
        }
        if (node.getJsonArray() != null && !node.getJsonArray().isEmpty()) {
            for (Object o : node.getJsonArray()) {
                JSONObject jsonObject = (JSONObject) o;
                node.init(jsonObject);
                T t = loadListData(node);
                if (t != null) {
                    list.add(t);
                }
            }
        }
        if (needSwap(list)) {
            StringUtil.swapList(list);
        }
        return list;
    }
}

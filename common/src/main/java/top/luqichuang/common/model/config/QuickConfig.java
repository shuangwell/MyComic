package top.luqichuang.common.model.config;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.model.builder.ChapterInfoBuilder;
import top.luqichuang.common.model.builder.EntityInfoBuilder;
import top.luqichuang.common.starter.ElementNode;
import top.luqichuang.common.starter.ElementStarter;
import top.luqichuang.common.util.SourceHelper;

/**
 * @author 18472
 * @desc
 * @date 2024/3/28 10:28
 * @ver 1.0
 */
public interface QuickConfig extends Source {

//    default Request getSearchRequest(String url, String searchString) {
//        if (url != null && url.contains("%s")) {
//            return NetUtil.getRequest(String.format(url, searchString));
//        } else {
//            return NetUtil.getRequest(url + searchString);
//        }
//    }

    @Override
    default List<EntityInfo> getInfoList(String html) {
        List<EntityInfo> list = new ArrayList<>();
        InfoListConfig config = getInfoListConfig();
        if (config != null) {
            List<ListConfig<EntityInfo>> configList = config.getConfigList();
            for (ListConfig<EntityInfo> listConfig : configList) {
                if (listConfig != null) {
                    list.addAll(new ElementStarter<EntityInfo>() {
                        @Override
                        protected EntityInfo loadListData(ElementNode node) {
                            return listConfig.getData(node);
                        }
                    }.startList(html, listConfig.getQuery()));
                }
            }
        }
        return list;
    }

    @Override
    default void setInfoDetail(EntityInfo info, String html, Map<String, Object> map) {
        InfoDetailConfig config = getInfoDetailConfig();
        if (config != null) {
            //设置Info
            new ElementStarter<EntityInfo>() {
                @Override
                protected EntityInfo loadData(ElementNode node) {
                    config.setData(info, node);
                    return super.loadData(node);
                }
            }.startData(config.getInfoHtml(html, map));

            //设置list
            List<ChapterInfo> list = new ArrayList<>();
            List<ListConfig<ChapterInfo>> configList = config.getConfigList();
            for (ListConfig<ChapterInfo> listConfig : configList) {
                if (listConfig != null) {
                    list.addAll(new ElementStarter<ChapterInfo>() {
                        @Override
                        protected boolean needSwap(List<ChapterInfo> list) {
                            return listConfig.needSwap(list);
                        }

                        @Override
                        protected ChapterInfo loadListData(ElementNode node) {
                            return listConfig.getData(node);
                        }
                    }.startList(config.getListHtml(html, map), listConfig.getQuery()));
                }
            }
            SourceHelper.initChapterInfoList(info, list);
        }
    }

    @Override
    default Map<String, String> getRankMap() {
        Map<String, String> map = new LinkedHashMap<>();
        RankMapConfig config = getRankMapConfig();
        if (config != null) {
            ElementNode node = new ElementNode(config.getHtml());
            Elements elements = node.getElements(config.getCssQuery());
            for (Element element : elements) {
                node.init(element);
                map.put(config.getTitle(node), config.getUrl(node));
            }
        }
        return map;
    }

    @Override
    default List<EntityInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }

    /*============================================================================================*/
    default InfoListConfig getInfoListConfig() {
        return new InfoListConfig() {
            @Override
            protected void addListConfig(List<ListConfig<EntityInfo>> configList) {
                configList.add(new ListConfig<EntityInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"null"};
                    }

                    @Override
                    protected EntityInfo getData(ElementNode node) {
                        return new EntityInfoBuilder(getInfoClass())
                                .buildSourceId(getSourceId())
                                .buildTitle(null)
                                .buildAuthor(null)
                                .buildUpdateTime(null)
                                .buildUpdateChapter(null)
                                .buildImgUrl(null)
                                .buildDetailUrl(null)
                                .build();
                    }
                });
            }
        };
    }

    default InfoDetailConfig getInfoDetailConfig() {
        return new InfoDetailConfig() {
            @Override
            protected void setData(EntityInfo info, ElementNode node) {
                new EntityInfoBuilder(info)
                        .buildSourceId(getSourceId())
                        .buildTitle(null)
                        .buildAuthor(null)
                        .buildIntro(null)
                        .buildUpdateTime(null)
                        .buildUpdateStatus(null)
                        .buildImgUrl(null)
                        .build();
            }

            @Override
            protected void addListConfig(List<ListConfig<ChapterInfo>> configList) {
                configList.add(new ListConfig<ChapterInfo>() {
                    @Override
                    protected String[] getQuery() {
                        return new String[]{"null"};
                    }

                    @Override
                    protected ChapterInfo getData(ElementNode node) {
                        return new ChapterInfoBuilder()
                                .buildTitle(node.ownText("a"))
                                .buildUrl(node.href("a"))
                                .build();
                    }
                });
            }
        };
    }

    default RankMapConfig getRankMapConfig() {
        return new RankMapConfig() {
            @Override
            protected String getHtml() {
                return "null";
            }

            @Override
            protected String getCssQuery() {
                return "null";
            }

            @Override
            protected String getTitle(ElementNode node) {
                return node.ownText("a");
            }

            @Override
            protected String getUrl(ElementNode node) {
                return node.href("a");
            }
        };
    }

    default InfoListConfig getRankListConfig() {
        return getInfoListConfig();
    }

    /*============================================================================================*/
    abstract class ListConfig<T> {
        protected boolean needSwap(List<T> list) {
            return false;
        }

        protected abstract String[] getQuery();

        protected abstract T getData(ElementNode node);
    }

    /*============================================================================================*/
    abstract class InfoListConfig {
        private List<ListConfig<EntityInfo>> configList = new ArrayList<>();

        public List<ListConfig<EntityInfo>> getConfigList() {
            if (configList.isEmpty()) {
                addListConfig(configList);
            }
            return configList;
        }

        protected abstract void addListConfig(List<ListConfig<EntityInfo>> configList);
    }

    /*============================================================================================*/
    abstract class InfoDetailConfig {
        private List<ListConfig<ChapterInfo>> configList = new ArrayList<>();

        public List<ListConfig<ChapterInfo>> getConfigList() {
            if (configList.isEmpty()) {
                addListConfig(configList);
            }
            return configList;
        }

        protected String getInfoHtml(String html, Map<String, Object> map) {
            return html;
        }

        protected String getListHtml(String html, Map<String, Object> map) {
            return html;
        }

        protected abstract void setData(EntityInfo info, ElementNode node);

        protected abstract void addListConfig(List<ListConfig<ChapterInfo>> configList);
    }

    /*============================================================================================*/
    abstract class RankMapConfig {
        protected abstract String getHtml();

        protected abstract String getCssQuery();

        protected abstract String getTitle(ElementNode node);

        protected abstract String getUrl(ElementNode node);
    }

}

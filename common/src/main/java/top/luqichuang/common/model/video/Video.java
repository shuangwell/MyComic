package top.luqichuang.common.model.video;

import top.luqichuang.common.model.Entity;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/15 14:23
 * @ver 1.0
 */
public class Video extends Entity {
    @Override
    public String toString() {
        return "Video{" +
                "id=" + id +
                ", sourceId=" + sourceId +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", status=" + status +
                ", info=" + info +
                ", infoList=" + infoList +
                ", priority=" + priority +
                ", isUpdate=" + isUpdate +
                ", date=" + date +
                '}';
    }
}

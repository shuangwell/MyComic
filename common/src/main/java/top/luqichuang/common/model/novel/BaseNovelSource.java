package top.luqichuang.common.model.novel;

import top.luqichuang.common.en.NSourceEnum;
import top.luqichuang.common.model.BaseSource;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 16:53
 * @ver 1.0
 */
public abstract class BaseNovelSource extends BaseSource {

    @Override
    public int getSourceType() {
        return NOVEL;
    }

    @Override
    public Class<? extends Entity> getEntityClass() {
        return Novel.class;
    }

    @Override
    public Class<? extends EntityInfo> getInfoClass() {
        return NovelInfo.class;
    }

    public abstract NSourceEnum getNSourceEnum();

    @Override
    public int getSourceId() {
        return getNSourceEnum().ID;
    }

    @Override
    public String getSourceName() {
        return getNSourceEnum().NAME;
    }
}

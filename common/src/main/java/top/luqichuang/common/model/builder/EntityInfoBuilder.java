package top.luqichuang.common.model.builder;

import java.util.List;

import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.util.SourceHelper;

/**
 * @author 18472
 * @desc
 * @date 2024/3/24 14:20
 * @ver 1.0
 */
public class EntityInfoBuilder {

    private EntityInfo info;

    public EntityInfoBuilder(Class<? extends EntityInfo> clazz) {
        try {
            this.info = clazz.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException(e);
        }
    }

    public EntityInfoBuilder() {
        this.info = new EntityInfo();
    }

    public EntityInfoBuilder(EntityInfo info) {
        this.info = info;
    }

    public EntityInfo build() {
        if (info.getTitle() != null) {
            return info;
        } else {
            return null;
        }
    }

    public EntityInfoBuilder buildSourceId(int sourceId) {
        if (sourceId != 0) {
            info.setSourceId(sourceId);
        }
        return this;
    }

    public EntityInfoBuilder buildTitle(String title) {
        if (title != null) {
            info.setTitle(title);
        }
        return this;
    }

    public EntityInfoBuilder buildAuthor(String author) {
        if (author != null) {
            info.setAuthor(author);
        }
        return this;
    }

    public EntityInfoBuilder buildUpdateTime(String updateTime) {
        if (updateTime != null) {
            info.setUpdateTime(updateTime);
        }
        return this;
    }

    public EntityInfoBuilder buildUpdateChapter(String updateChapter) {
        if (updateChapter != null) {
            info.setUpdateChapter(updateChapter);
        }
        return this;
    }

    public EntityInfoBuilder buildUpdateStatus(String updateStatus) {
        if (updateStatus != null) {
            info.setUpdateStatus(updateStatus);
        }
        return this;
    }

    public EntityInfoBuilder buildIntro(String intro) {
        if (intro != null) {
            info.setIntro(intro);
        }
        return this;
    }

    public EntityInfoBuilder buildImgUrl(String imgUrl) {
        if (imgUrl != null) {
            info.setImgUrl(imgUrl);
        }
        return this;
    }

    public EntityInfoBuilder buildDetailUrl(String detailUrl) {
        if (detailUrl != null) {
            info.setDetailUrl(detailUrl);
        }
        return this;
    }

    public EntityInfoBuilder buildChapterInfoList(List<ChapterInfo> chapterInfoList) {
        if (info.getChapterInfoList().isEmpty()) {
            SourceHelper.initChapterInfoList(info, chapterInfoList);
        }
        return this;
    }

}

package top.luqichuang.common.en;

import java.util.LinkedHashMap;
import java.util.Map;

import top.luqichuang.common.model.Source;
import top.luqichuang.common.source.comic.AiYouMan;
import top.luqichuang.common.source.comic.BaiManWu;
import top.luqichuang.common.source.comic.BaoZi;
import top.luqichuang.common.source.comic.BiliBili;
import top.luqichuang.common.source.comic.Cui;
import top.luqichuang.common.source.comic.DaShu;
import top.luqichuang.common.source.comic.Ding;
import top.luqichuang.common.source.comic.Du;
import top.luqichuang.common.source.comic.DuShi;
import top.luqichuang.common.source.comic.GoDa;
import top.luqichuang.common.source.comic.Gou;
import top.luqichuang.common.source.comic.HaoGuoMan;
import top.luqichuang.common.source.comic.KaiXin;
import top.luqichuang.common.source.comic.Kuang;
import top.luqichuang.common.source.comic.Lai;
import top.luqichuang.common.source.comic.MH100;
import top.luqichuang.common.source.comic.MH118;
import top.luqichuang.common.source.comic.MH118W;
import top.luqichuang.common.source.comic.MH1234;
import top.luqichuang.common.source.comic.MH160;
import top.luqichuang.common.source.comic.MH166;
import top.luqichuang.common.source.comic.MH4499;
import top.luqichuang.common.source.comic.MH6;
import top.luqichuang.common.source.comic.ManHuaFen;
import top.luqichuang.common.source.comic.ManHuaTai;
import top.luqichuang.common.source.comic.MiTui;
import top.luqichuang.common.source.comic.OH;
import top.luqichuang.common.source.comic.PinYue;
import top.luqichuang.common.source.comic.PuFei;
import top.luqichuang.common.source.comic.QiMiao;
import top.luqichuang.common.source.comic.QiXi;
import top.luqichuang.common.source.comic.QianWei;
import top.luqichuang.common.source.comic.SiSi;
import top.luqichuang.common.source.comic.TengXun;
import top.luqichuang.common.source.comic.XianMan;
import top.luqichuang.common.source.comic.ZhuZhu;
import top.luqichuang.mycomic.source.BL;
import top.luqichuang.mycomic.source.HaoMan6;
import top.luqichuang.mycomic.source.KuManWu;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/1/11 23:51
 * @ver 1.0
 */
public enum CSourceEnum {
    EMPTY_SOURCE(0, "空", null),
    MI_TUI(1, "米推漫画", new MiTui()),
    MAN_HUA_FEN(2, "漫画粉", new ManHuaFen()),
    PU_FEI(3, "扑飞漫画", new PuFei()),
    TENG_XUN(4, "腾讯动漫", new TengXun()),
    BILI_BILI(5, "哔哩哔哩", new BiliBili()),
    OH(6, "OH漫画", new OH()),
    MAN_HUA_TAI(7, "漫画台", new ManHuaTai()),
    MH_118(8, "118漫画", new MH118()),
    DU(9, "独漫画", new Du()),
    BL(10, "BL漫画", new BL()),
    AI_YOU_MAN(11, "爱优漫", new AiYouMan()),
    MH_1234(12, "1234漫画", new MH1234()),
    MH_118_2(13, "118漫画[2]", new MH118W()),
    QI_MIAO(14, "奇妙漫画", new QiMiao()),
    DA_SHU(15, "大树漫画", new DaShu()),
    SI_SI(16, "思思漫画", new SiSi()),
    BAO_ZI(17, "包子漫画", new BaoZi()),
    QI_XI(18, "七夕漫画", new QiXi()),
    KU_MAN_WU(19, "酷漫屋", new KuManWu()),
    HAO_MAN_6(20, "好漫6", new HaoMan6()),
    DU_SHI(21, "都市漫画", new DuShi()),
    QIAN_WEI(22, "前未漫画", new QianWei()),
    PIN_YUE(23, "品悦漫画", new PinYue()),
    LAI(24, "来漫画", new Lai()),
    MH_6(25, "6漫画", new MH6()),
    MH_160(26, "160漫画", new MH160()),
    BAI_MAN_WU(27, "百漫屋", new BaiManWu()),
    GO_DA(28, "GoDa漫画", new GoDa()),
    KAI_XIN(29, "开心漫画", new KaiXin()),
    CUI(30, "催漫画", new Cui()),
    MH_166(31, "妖六六", new MH166()),
    MH_100(32, "漫画连", new MH100()),
    KUANG(33, "动漫狂", new Kuang()),
    MH_4499(34, "漫画4499", new MH4499()),
    XIAN_MAN(35, "仙漫网", new XianMan()),
    ZHU_ZHU(36, "猪猪漫画", new ZhuZhu()),
    GOU(37, "漫画狗", new Gou()),
    HAO_GUO_MAN(38, "好国漫", new HaoGuoMan()),
    DING(39, "顶漫画", new Ding()),
    ;

    private static final Map<Integer, Source> MAP = new LinkedHashMap<>();

    static {
        for (CSourceEnum value : values()) {
            if (value.SOURCE != null && value.SOURCE.isValid()) {
                MAP.put(value.ID, value.SOURCE);
            }
        }
    }

    public static Map<Integer, Source> getMAP() {
        return MAP;
    }

    public final int ID;
    public final String NAME;
    public final Source SOURCE;

    CSourceEnum(int id, String name, Source source) {
        this.ID = id;
        this.NAME = name;
        this.SOURCE = source;
    }
}

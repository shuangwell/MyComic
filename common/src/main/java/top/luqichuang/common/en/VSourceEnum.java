package top.luqichuang.common.en;

import java.util.LinkedHashMap;
import java.util.Map;

import top.luqichuang.common.model.Source;
import top.luqichuang.common.source.video.AiYun;
import top.luqichuang.common.source.video.BiliBili;
import top.luqichuang.common.source.video.FengChe;
import top.luqichuang.common.source.video.FengChe2;
import top.luqichuang.common.source.video.MiLiMiLi;
import top.luqichuang.common.source.video.YingHua;
import top.luqichuang.common.source.video.YingHua2;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/22 23:47
 * @ver 1.0
 */
public enum VSourceEnum {
    EMPTY_SOURCE(0, "空", null),
    YING_HUA(1, "樱花动漫", new YingHua()),
    MILI_MILI(2, "米粒米粒", new MiLiMiLi()),
    FENG_CHE(3, "风车动漫", new FengChe()),
    YING_HUA_2(4, "樱花动漫[2]", new YingHua2()),
    BILI_BILI(5, "哔哩哔哩", new BiliBili()),
    FENG_CHE_2(6, "风车动漫[2]", new FengChe2()),
    AI_YUN(7, "爱云影视", new AiYun()),
    ;

    private static final Map<Integer, Source> MAP = new LinkedHashMap<>();

    static {
        for (VSourceEnum value : values()) {
            if (value.SOURCE != null && value.SOURCE.isValid()) {
                MAP.put(value.ID, (Source) value.SOURCE);
            }
        }
    }

    public static Map<Integer, Source> getMAP() {
        return MAP;
    }

    public final int ID;
    public final String NAME;
    public final Source SOURCE;

    VSourceEnum(int id, String name, Source source) {
        this.ID = id;
        this.NAME = name;
        this.SOURCE = source;
    }
}

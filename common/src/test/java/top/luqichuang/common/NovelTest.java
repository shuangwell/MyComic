package top.luqichuang.common;

import org.junit.Test;

import top.luqichuang.common.model.Source;
import top.luqichuang.common.source.novel.AiYue;
import top.luqichuang.common.tst.BaseSourceTest;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/7/9 17:25
 * @ver 1.0
 */
public class NovelTest extends BaseSourceTest {
    @Override
    protected Source getSource() {
        return new AiYue();
    }

    @Test
    @Override
    public void testRequest() {
        String dRequest = "https://www.biqupai.com/78_78513/";
        String cRequest = "https://www.biqupai.com/78_78513/122951.html";

//        testSearchRequest();
//        testSearch();
//        testDetailRequest(dRequest);
//        testDetail();
//        testContentRequest(cRequest);
//        testContent();
//        testRankMap();
//        testRankRequest();
//        testRank();

//        autoTest();

        allTest();
    }
}

package com.qc.common;

import android.app.Activity;

import com.qc.common.ui.activity.CrashActivity;
import com.qc.common.ui.activity.LauncherActivity;

import java.util.ArrayList;
import java.util.List;

import the.one.base.BaseApplication;
import the.one.base.util.crash.CrashConfig;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/10/28 11:05
 * @ver 1.0
 */
public class MyBaseApplication extends BaseApplication {

    private static final List<Activity> ACTIVITY_LIST = new ArrayList<>();

    @Override
    protected Class getStartActivity() {
        return LauncherActivity.class;
    }

    @Override
    protected void initCrashConfig() {
        super.initCrashConfig();
        CrashConfig.Builder.create()
                .backgroundMode(CrashConfig.BACKGROUND_MODE_SHOW_CUSTOM)
                .enabled(true)
                .trackActivities(true)
                .minTimeBetweenCrashesMs(2000)
                // 重启的 Activity
                .restartActivity(LauncherActivity.class)
                // 错误的 Activity
                .errorActivity(CrashActivity.class)
                // 设置监听器
//                .eventListener(new YourCustomEventListener())
                .apply();
    }

    public static void addActivity(Activity activity) {
        ACTIVITY_LIST.add(activity);
    }

    public static void finishActivity() {
        for (Activity activity : ACTIVITY_LIST) {
            if (activity != null) {
                activity.finish();
            }
        }
    }
}

package com.qc.common.util;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.qc.common.MyBaseApplication;
import com.qc.common.ui.activity.LauncherActivity;

/**
 * @author LuQiChuang
 * @desc 重启工具
 * @date 2020/8/18 10:02
 * @ver 1.0
 */
public class RestartUtil {

    public static final String NEED_RESTART = "needRestart";

    public static void restart(Activity activity) {
        Intent intent = new Intent(activity, LauncherActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        activity.finish();
        activity.startActivity(intent);
//        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private static void restart(Context context) {
        Application application = (Application) context.getApplicationContext();
        Intent intent = new Intent(application, LauncherActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        MyBaseApplication.finishActivity();
        application.startActivity(intent);
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public static void restart() {
        restart(MyBaseApplication.getInstance());
    }

}

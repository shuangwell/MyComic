package com.qc.common.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.Headers;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.qc.common.MyBaseApplication;
import com.qc.common.en.data.Data;
import com.qc.common.self.ImageConfig;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.QMUIProgressBar;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;
import the.one.base.Interface.GlideProgressListener;
import the.one.base.util.glide.GlideProgressInterceptor;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;

/**
 * @author 18472
 * @desc
 * @date 2024/2/25 10:30
 * @ver 1.0
 */
public class ImageUtil {

    private static final Map<String, Integer> STATE_MAP = new HashMap<>();
    private static final Map<String, Integer> PROGRESS_MAP = new HashMap<>();
    private static final Map<String, Target<?>> TARGET_MAP = new HashMap<>();
    private static final Set<String> CACHE_SET = new HashSet<>();
    private static final Queue<ImageConfig> PRELOAD_QUEUE = new LinkedList<>();

    private static final int CACHE_NUM = 5;

    public static final int LOAD_ING = 1;
    public static final int LOAD_SUCCESS = 2;
    public static final int LOAD_FAIL = 3;

    private static int defaultHeight = -1;
    private static int screenWidth = -1;

    private static Context getContext() {
        return MyBaseApplication.getInstance();
    }

    /**
     * @return int
     * @desc 获取屏幕宽度
     */
    private static int getScreenWidth() {
        if (screenWidth == -1) {
            screenWidth = QMUIDisplayHelper.getScreenWidth(getContext());
        }
        return screenWidth;
    }

    private static int getDefaultHeight() {
        if (defaultHeight == -1) {
            defaultHeight = QMUIDisplayHelper.dp2px(getContext(), 300);
        }
        return defaultHeight;
    }

    public static ImageConfig getDefaultConfig(String url) {
        ImageConfig config = new ImageConfig();
        config.setUrl(url);
        config.setDefaultBitmapId(R.drawable.ic_image_none);
        config.setErrorBitmapId(R.drawable.ic_image_none);
        config.setDrawableId(R.drawable.ic_image_background);
        config.setRead(false);
        return config;
    }

    public static ImageConfig getReaderConfig(String url) {
        ImageConfig config = new ImageConfig();
        config.setUrl(url);
        config.setDefaultBitmapId(0);
        config.setErrorBitmapId(R.drawable.ic_image_error_24);
        config.setDrawableId(R.drawable.ic_image_reader_background);
        config.setRead(true);
        return config;
    }

    public static ImageConfig.View initView(RelativeLayout layout) {
        if (layout == null) {
            return new ImageConfig.View();
        }
        if (layout.getTag() == null) {
            ImageConfig.View view = new ImageConfig.View();
            ImageView imageView = layout.findViewById(R.id.imageView);
            QMUIProgressBar progressBar = layout.findViewById(R.id.progressBar);
            TextView textView = layout.findViewById(R.id.textView);
            view.setLayout(layout);
            view.setImageView(imageView);
            view.setProgressBar(progressBar);
            view.setTextView(textView);
            layout.setTag(view);
        }
        return (ImageConfig.View) layout.getTag();
    }

    private static void initRequest(ImageConfig config, ImageConfig.View view) {
        String imageTag = (String) view.getImageView().getTag();
        if (imageTag != null && !imageTag.equals(config.getUrl())) {
            Glide.with(getContext()).clear(TARGET_MAP.get(imageTag));
            TARGET_MAP.remove(imageTag);
            PROGRESS_MAP.remove(imageTag);
        }
    }

    public static void loadImage(ImageConfig config, RelativeLayout layout) {
        loadImage(config, initView(layout));
    }

    public static void loadImage(ImageConfig config, ImageConfig.View view) {
        if (view.getLayout() == null) {
            return;
        }
        initRequest(config, view);
        view.getImageView().setTag(config.getUrl());
        view.getProgressBar().setTag(config.getUrl());
        view.getTextView().setTag(config.getUrl());
        view.getProgressBar().setVisibility(View.INVISIBLE);
        view.getTextView().setVisibility(View.INVISIBLE);
        if (config.getUrl() == null || config.getUrl().isEmpty()) {
            view.getImageView().setImageBitmap(getBitmap(config.getErrorBitmapId()));
            view.getImageView().setBackground(getDrawable(config.getDrawableId()));
            return;
        }
        view.getImageView().setImageBitmap(getBitmap(config.getDefaultBitmapId()));
        view.getImageView().setBackground(getDrawable(config.getDrawableId()));
        if (!config.isForce() && loadImageLocal(config, view)) {
            return;
        }
        loadImageNet(config, view);
    }

    public static boolean loadImageLocal(ImageConfig config, ImageConfig.View view) {
        if (config.isSave() && config.getSaveKey() != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            File file = new File(getLocalImgUrl(config.getSaveKey()));
            if (file.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
                if (bitmap != null) {
                    view.getImageView().setImageBitmap(bitmap);
                    view.getImageView().setScaleType(ImageView.ScaleType.FIT_XY);
                    return true;
                }
            }
        }
        return false;
    }

    public static void loadImageNet(ImageConfig config, ImageConfig.View view) {
        GlideUrl glideUrl = getGlideUrl(config, config.getUrl());
        addInterceptor(config, view);

        RelativeLayout layout = view.getLayout();
        ImageView imageView = view.getImageView();
        QMUIProgressBar progressBar = view.getProgressBar();
        TextView textView = view.getTextView();

        Target<?> target = Glide.with(getContext())
                .asBitmap()
                .placeholder(config.getDrawableId())
                .error(config.getDrawableId())
                .load(glideUrl)
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .dontAnimate()
                .priority(config.isForce() ? Priority.IMMEDIATE : Priority.HIGH)
                .addListener(getRequestListener(config))
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onLoadStarted(@Nullable Drawable placeholder) {
                        if (config.getUrl().equals(imageView.getTag())) {
                            imageView.setImageBitmap(getBitmap(config.getDefaultBitmapId()));
                            imageView.setBackground(placeholder);
                            if (config.isRead()) {
                                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                setLP(imageView, getScreenWidth(), getDefaultHeight());
                                setLP(layout, getScreenWidth(), getDefaultHeight());
                            }
                        }
                        if (config.getUrl().equals(view.getProgressBar().getTag())) {
                            Integer progress = PROGRESS_MAP.get(config.getUrl());
                            int num = progress == null ? 0 : progress;
                            String text = num + "%";
                            view.getProgressBar().setVisibility(View.VISIBLE);
                            view.getTextView().setVisibility(View.VISIBLE);
                            view.getProgressBar().setProgress(num, false);
                            view.getTextView().setText(text);
                        }
                        STATE_MAP.put(config.getUrl(), LOAD_ING);
                    }

                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        if (config.getUrl().equals(imageView.getTag())) {
                            imageView.setImageBitmap(resource);
                            if (config.isRead()) {
                                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                setLP(imageView, resource);
                                setLP(layout, resource);
                            }
                        }
                        if (config.getUrl().equals(progressBar.getTag())) {
                            progressBar.setVisibility(View.INVISIBLE);
                            textView.setVisibility(View.INVISIBLE);
                        }
                        if (config.isSave() && config.getSaveKey() != null) {
                            saveBitmapBackPath(resource, config.getSaveKey());
                        }
                        STATE_MAP.put(config.getUrl(), LOAD_SUCCESS);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        if (config.getUrl().equals(imageView.getTag())) {
                            imageView.setImageBitmap(getBitmap(config.getErrorBitmapId()));
                            imageView.setBackground(errorDrawable);
                            if (config.isRead()) {
                                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                                setLP(imageView, getScreenWidth(), getDefaultHeight());
                                setLP(layout, getScreenWidth(), getDefaultHeight());
                            }
                        }
                        if (config.getUrl().equals(progressBar.getTag())) {
                            progressBar.setVisibility(View.INVISIBLE);
                            textView.setVisibility(View.INVISIBLE);
                        }
                        STATE_MAP.put(config.getUrl(), LOAD_FAIL);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                        onLoadStarted(placeholder);
                    }
                });
        TARGET_MAP.put(config.getUrl(), target);
    }

    public static void preloadImage(ImageConfig config) {
        if (config != null) {
            PRELOAD_QUEUE.offer(config);
            preloadImageStart();
        }
    }

    public static void clearCacheMap() {
        PROGRESS_MAP.clear();
        STATE_MAP.clear();
        CACHE_SET.clear();
        PRELOAD_QUEUE.clear();
        for (Target<?> target : TARGET_MAP.values()) {
            Glide.with(getContext()).clear(target);
        }
        TARGET_MAP.clear();
        Glide.with(getContext()).onStop();
        Glide.with(getContext()).onStart();
    }

    /**
     * 根据url获得图片加载状态
     *
     * @param content content
     * @return int
     */
    public static int getLoadStatus(Content content) {
        if (content != null) {
            return getLoadStatus(content.getUrl());
        }
        return LOAD_ING;
    }

    /**
     * 根据url获得图片加载状态
     *
     * @param url url
     * @return int
     */
    public static int getLoadStatus(String url) {
        if (url != null) {
            Integer status = STATE_MAP.get(url);
            if (status != null) {
                return status;
            }
        }
        return LOAD_ING;
    }

    public static void setSaveKey(Entity entity, ImageConfig config) {
        if (entity.getInfo().getSourceId() == 0) {
            config.setSaveKey(null);
        } else {
            if (Data.contentCode == Data.COMIC_CODE) {
                config.setSaveKey("C_" + entity.getTitle() + "_" + entity.getSourceId());
            } else if (Data.contentCode == Data.NOVEL_CODE) {
                config.setSaveKey("N_" + entity.getTitle() + "_" + entity.getSourceId());
            } else {
                config.setSaveKey("V_" + entity.getTitle() + "_" + entity.getSourceId());
            }
        }
    }

    public static void pauseLoad() {
        Glide.with(getContext()).pauseRequests();
    }

    public static void resumeLoad() {
        Glide.with(getContext()).resumeRequests();
    }

    /*=============================================================================*/
    private static void addInterceptor(ImageConfig config, ImageConfig.View view) {
        if (view == null && GlideProgressInterceptor.LISTENER_MAP.containsKey(config.getUrl())) {
            return;
        }
        GlideProgressInterceptor.LISTENER_MAP.put(config.getUrl(), new GlideProgressListener() {
            @Override
            public void onProgress(int progress, boolean success) {
                Integer integer = PROGRESS_MAP.get(config.getUrl());
                if (integer != null) {
                    progress = Math.max(progress, integer);
                }
                progress = Math.max(progress, 1);
                progress = Math.min(progress, 99);
                PROGRESS_MAP.put(config.getUrl(), progress);
//                System.out.printf(Locale.CHINA, "[%02d] -> %s\n", progress, config.getUrl());
                if (view == null || view.getProgressBar() == null) {
                    return;
                }
//                System.out.printf(Locale.CHINA, "[%5s] [%02d] -> %s\n", config.getUrl().equals(view.getProgressBar().getTag()), progress, config.getUrl());
                if (config.getUrl().equals(view.getProgressBar().getTag())) {
                    if (getLoadStatus(config.getUrl()) != LOAD_FAIL) {
                        int num = progress;
                        String text = num + "%";
                        AndroidSchedulers.mainThread().scheduleDirect(() -> {
                            view.getProgressBar().setVisibility(View.VISIBLE);
                            view.getTextView().setVisibility(View.VISIBLE);
                            view.getProgressBar().setProgress(num);
                            view.getTextView().setText(text);
                        });
                    } else {
                        if (view.getProgressBar().getVisibility() == View.VISIBLE) {
                            AndroidSchedulers.mainThread().scheduleDirect(() -> {
                                view.getProgressBar().setVisibility(View.INVISIBLE);
                                view.getTextView().setVisibility(View.INVISIBLE);
                            });
                        }
                    }
                }
            }
        });
    }

    private static GlideUrl getGlideUrl(ImageConfig config, String url) {
        return new GlideUrl(url, () -> {
            if (config.getHeaders() != null) {
                return config.getHeaders();
            } else {
                return Headers.DEFAULT.getHeaders();
            }
        });
    }

    private static <T> RequestListener<T> getRequestListener(ImageConfig config) {
        return new RequestListener<T>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<T> target, boolean isFirstResource) {
                PRELOAD_QUEUE.offer(config);
                onLoadEnd();
                return false;
            }

            @Override
            public boolean onResourceReady(T resource, Object model, Target<T> target, DataSource dataSource, boolean isFirstResource) {
                onLoadEnd();
                return false;
            }

            private void onLoadEnd() {
                TARGET_MAP.remove(config.getUrl());
                PROGRESS_MAP.remove(config.getUrl());
                GlideProgressInterceptor.removeListener(config.getUrl());
                if (!CACHE_SET.isEmpty()) {
                    CACHE_SET.remove(config.getUrl());
                    PRELOAD_QUEUE.remove(config);
                    preloadImageStart();
                }
            }
        };
    }

    private static void preloadImageStart() {
        if (!PRELOAD_QUEUE.isEmpty() && CACHE_SET.size() < CACHE_NUM) {
            ImageConfig config = PRELOAD_QUEUE.poll();
            if (config != null) {
                CACHE_SET.add(config.getUrl());
                addInterceptor(config, null);
                GlideUrl glideUrl = getGlideUrl(config, config.getUrl());
                Glide.with(getContext())
                        .load(glideUrl)
                        .priority(Priority.NORMAL)
//                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .addListener(getRequestListener(config))
                        .preload();
            }
        }
    }

    /*=============================================================================*/

    private static void setLP(View view, Bitmap bitmap) {
        ViewGroup.LayoutParams lp = view.getLayoutParams();
        int bWidth = bitmap.getWidth();
        int bHeight = bitmap.getHeight();
        int sWidth = getScreenWidth();
        int sHeight = bHeight * sWidth / bWidth;
        lp.width = sWidth;
        lp.height = sHeight;
        view.setLayoutParams(lp);
    }

    private static void setLP(View view, int width, int height) {
        ViewGroup.LayoutParams lp = view.getLayoutParams();
        lp.width = width;
        lp.height = height;
        view.setLayoutParams(lp);
    }

    /**
     * 保存bitmap到本地并返回地址
     *
     * @param bm  bm
     * @param key key
     * @return String
     */
    private static String saveBitmapBackPath(Bitmap bm, Object key) {
        String path = null;
        File savedFile = DBUtil.createFile(getLocalImgUrl(key));
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(savedFile));
            bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            path = savedFile.getAbsolutePath();
        } catch (Exception e) {
            savedFile.delete();
            e.printStackTrace();
        } finally {
            try {
                if (bos != null) {
                    bos.flush();
                    bos.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return path;
    }

    /*=============================================================================*/

    /**
     * 根据key获得本地图片地址
     *
     * @param key key
     * @return String
     */
    public static String getLocalImgUrl(Object key) {
        return Data.getImgPath() + "/img_" + key.toString();
    }

    /**
     * 根据id获得Drawable
     *
     * @param drawableId drawableId
     * @return Drawable
     */
    public static Drawable getDrawable(int drawableId) {
        if (drawableId == 0) {
            return null;
        }
        return ContextCompat.getDrawable(getContext(), drawableId);
    }

    /**
     * 根据id获得Bitmap
     *
     * @param bitmapId bitmapId
     * @return Bitmap
     */
    public static Bitmap getBitmap(int bitmapId) {
        return drawableToBitmap(getDrawable(bitmapId));
    }

    /**
     * Drawable -> Bitmap
     *
     * @param drawable drawable
     * @return Bitmap
     */
    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else {
            return drawableToBitmapByCanvas(drawable);
        }
    }

    /**
     * Bitmap -> Drawable
     *
     * @param bitmap bitmap
     * @return Drawable
     */
    public static Drawable bitmapToDrawable(Bitmap bitmap) {
        return new BitmapDrawable(getContext().getResources(), bitmap);
    }

    /**
     * Drawable -> Bitmap
     *
     * @param drawable drawable
     * @return Bitmap
     */
    public static Bitmap drawableToBitmapByCanvas(Drawable drawable) {
        int width = drawable.getIntrinsicWidth() > 0 ? drawable.getIntrinsicWidth() : 100;
        int height = drawable.getIntrinsicHeight() > 0 ? drawable.getIntrinsicHeight() : 100;
        Bitmap bitmap = Bitmap.createBitmap(width, height,
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    /**
     * byte[] -> Bitmap
     *
     * @param bytes      bytes
     * @param isCompress isCompress
     * @return Bitmap
     */
    private static Bitmap bytesToBitmap(byte[] bytes, boolean isCompress) {
        Bitmap bitmap;
        if (isCompress) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inSampleSize = 2;
            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
        } else {
            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }
        //Log.i("TAG", "bytesToBitmap: bitmap.cSize = " + bitmap.getByteCount() / 1024 + "KB");
        return bitmap;
    }

    /**
     * 图片压缩
     *
     * @param bitmap bitmap
     * @return Bitmap
     */
    private static Bitmap compressBitmap(Bitmap bitmap) {
        int length = bitmap.getByteCount();
        if (length / 1024 > 2000) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inSampleSize = 2;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
            bitmap = BitmapFactory.decodeStream(isBm, null, options);
        }
        return bitmap;
    }

    /**
     * 计算bitmap大小
     *
     * @param bitmap bitmap
     * @return int
     */
    public static int getBitmapSize(Bitmap bitmap) {
        return bitmap.getByteCount();
    }

}

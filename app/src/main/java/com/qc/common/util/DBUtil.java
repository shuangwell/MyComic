package com.qc.common.util;

import android.content.Context;

import com.qc.common.MyBaseApplication;
import com.qc.common.en.data.Data;

import org.litepal.LitePal;
import org.litepal.LitePalApplication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import top.luqichuang.common.model.CacheData;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.CacheUtil;
import top.luqichuang.common.util.DateUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/9 19:07
 * @ver 1.0
 */
public class DBUtil {

    public static final String TAG = "DBUtil";

    public static final int SAVE_ONLY = 0;
    public static final int SAVE_CUR = 1;
    public static final int SAVE_ALL = 2;

    private static final ExecutorService POOL = Executors.newFixedThreadPool(3);

    public static void initLitePal() {
        try {
            LitePalApplication.getContext();
        } catch (Exception e) {
            LitePal.initialize(MyBaseApplication.getInstance());
        }
    }

    public static void createDirs(String path) {
        createDirs(new File(path));
    }

    public static void createDirs(File file) {
        if (file != null && !file.exists()) {
            file.mkdirs();
        }
    }

    public static File createFile(String path) {
        return createFile(new File(path));
    }

    private static File createFile(String path, String fileName) {
        File file = new File(path, fileName);
        return createFile(file);
    }

    public static File createFile(File file) {
        try {
            if (file != null) {
                createDirs(file.getParentFile());
                if (!file.exists()) {
                    file.createNewFile();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void saveOnly(Entity entity) {
        save(entity, SAVE_ONLY);
    }

    public static void saveCur(Entity entity) {
        save(entity, SAVE_CUR);
    }

    public static void saveAll(Entity entity) {
        save(entity, SAVE_ALL);
    }

    public static void save(Entity entity, int mode) {
        if (entity != null) {
            saveEntityData(entity);
            if (mode == SAVE_CUR) {
                saveInfoData(entity.getInfo());
            } else if (mode == SAVE_ALL) {
                for (EntityInfo info : entity.getInfoList()) {
                    saveInfoData(info);
                }
            }
        }
    }

    public static CacheData saveCache(String key, Object o) {
        return saveCache(key, o, CacheUtil.DEFAULT_EXPIRE);
    }

    public static CacheData saveCache(String key, Object o, long expire) {
        CacheData cacheData = CacheUtil.getCacheData(key, CacheUtil.toJson(o), expire);
        saveCache(cacheData);
        return cacheData;
    }

    public static void saveCache(CacheData cacheData) {
        if (cacheData != null && cacheData.getKey() != null) {
            POOL.execute(() -> {
                initLitePal();
                cacheData.saveOrUpdate("key = ?", cacheData.getKey());
            });
        }
    }

    public static CacheData findCache(String key) {
        initLitePal();
        return LitePal.where("key = ?", key).findFirst(CacheData.class);
    }

    public static void deleteCache(String key) {
        if (key != null) {
            initLitePal();
            LitePal.deleteAll(CacheData.class, "key = ?", key);
        }
    }

    public static void deleteCache(CacheData cacheData) {
        if (cacheData != null) {
            POOL.execute(() -> {
                initLitePal();
                cacheData.delete();
            });
        }
    }

    public static void deleteAllCache() {
        POOL.execute(() -> {
            initLitePal();
            LitePal.deleteAll(CacheData.class);
        });
    }

    public static void saveEntityData(Entity entity) {
        if (entity != null) {
            POOL.execute(() -> {
                initLitePal();
                if (entity.getTitle() == null) {
                    if (entity.getInfo() != null && entity.getInfo().getTitle() != null) {
                        entity.setTitle(entity.getInfo().getTitle());
                        entity.saveOrUpdate("title = ?", entity.getTitle());
                    }
                } else {
                    entity.saveOrUpdate("title = ?", entity.getTitle());
                }
            });
        }
    }

    public static void saveInfoData(EntityInfo info) {
        if (info != null) {
            POOL.execute(() -> {
                initLitePal();
                if (info.getTitle() != null) {
                    if (info.getAuthor() == null) {
                        info.setAuthor("null");
                    }
                    info.saveOrUpdate("title = ? and sourceId = ? and author = ?", info.getTitle(), String.valueOf(info.getSourceId()), info.getAuthor());
                }
            });
        }
    }

    public static void deleteData(Entity entity) {
        if (entity != null) {
            POOL.execute(() -> {
                initLitePal();
                entity.delete();
                for (EntityInfo info : entity.getInfoList()) {
                    File file = new File(ImageUtil.getLocalImgUrl(info.getId()));
                    if (file.exists()) {
                        file.delete();
                    }
                }
            });
        }
    }

    public static List<Entity> findEntityList() {
        String order = "priority DESC, date DESC";
        Class<Entity> entityClass = SourceUtil.getEntityClass();
        Class<EntityInfo> infoClass = SourceUtil.getEntityInfoClass();
        initLitePal();
        List<EntityInfo> allInfoList = LitePal.order("sourceId ASC").find(infoClass);
        Map<String, List<EntityInfo>> infoMap = new HashMap<>();
        for (EntityInfo info : allInfoList) {
            List<EntityInfo> infoList = infoMap.get(info.getTitle());
            if (infoList == null) {
                infoList = new ArrayList<>();
            }
            infoList.add(info);
            infoMap.put(info.getTitle(), infoList);
        }
        List<Entity> entityList = LitePal.order(order).find(entityClass);
        List<Entity> dList = new ArrayList<>();
        for (Entity entity : entityList) {
            List<EntityInfo> infoList = infoMap.get(entity.getTitle());
            if (infoList == null) {
                dList.add(entity);
                deleteData(entity);
                continue;
            }
            for (EntityInfo info : infoList) {
                Source source = SourceUtil.getSource(info.getSourceId());
                if (!SourceUtil.getSourceList().contains(source)) {
                    source = null;
                }
                if (source != null && source.isValid()) {
                    EntityUtil.addInfo(entity, info);
                    if (entity.getSourceId() == info.getSourceId()) {
                        entity.setInfo(info);
                    }
                    //更改detailUrl
                    String url = info.getDetailUrl();
                    String index = source.getIndex();
                    if (!url.startsWith(index)) {
                        String tmp = url.substring(url.indexOf('/', url.indexOf('.')));
                        url = index + tmp;
                        info.setDetailUrl(url);
                        saveInfoData(info);
                    }
                    //end
                }
            }
            if (entity.getInfo() == null) {
                if (entity.getInfoList().isEmpty()) {
                    dList.add(entity);
                } else {
                    entity.setInfo(entity.getInfoList().get(0));
                    entity.setSourceId(entity.getInfoList().get(0).getSourceId());
                }
            }
            infoMap.remove(entity.getTitle());
        }
        if (!dList.isEmpty()) {
            entityList.removeAll(dList);
        }
        for (String title : infoMap.keySet()) {
            LitePal.deleteAll(infoClass, "title = ?", title);
        }
        return entityList;
    }

    public static void autoBackup(Context context) {
        if (!existAuto()) {
            backupData(context, getAutoName());
        }
        deleteAuto();
    }

    public static String getAutoName() {
        return Data.getAutoSavePath() + "/自动备份#" + DateUtil.formatAutoBackup(new Date());
    }

    public static boolean existAuto() {
        return new File(getAutoName()).exists();
    }

    public static boolean deleteAuto() {
        try {
            File file = new File(Data.getAutoSavePath());
            File[] files = file.listFiles();
            while (files.length > 5) {
                int old = 0;
                long oldVal = files[0].lastModified();
                for (int i = 1; i < files.length; i++) {
                    File f = files[i];
                    if (f.lastModified() < oldVal) {
                        old = i;
                        oldVal = f.lastModified();
                    }
                }
                files[old].delete();
                files = file.listFiles();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteShelfImg() {
        String path = Data.getImgPath();
        File file = new File(path);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    public static boolean backupData(Context context) {
        return backupData(context, Data.getSavePathName());
    }

    public static boolean backupData(Context context, String path) {
        File dbFile = context.getDatabasePath(Data.DB_FILE_NAME);
        File backupFile = createFile(path);
        return fileCopy(dbFile, backupFile);
    }

    public static boolean restoreData(Context context) {
        return restoreData(context, Data.getSavePathName());
    }

    public static boolean restoreData(Context context, String path) {
        File dbFile = context.getDatabasePath(Data.DB_FILE_NAME);
        File backupFile = createFile(path);
        File tmpFile = createFile(Data.getAppPath(), "tmp.db");
        fileCopy(dbFile, tmpFile);
        try {
            if (fileCopy(backupFile, dbFile)) {
                EntityUtil.init();
                tmpFile.delete();
                return true;
            }
        } catch (Exception e) {
            LitePal.getDatabase().close();
            fileCopy(tmpFile, dbFile);
            tmpFile.delete();
        }
        return false;
    }

    private static boolean fileCopy(File oFile, File toFile) {
        try (FileChannel inChannel = new FileInputStream(oFile).getChannel(); FileChannel outChannel = new FileOutputStream(toFile).getChannel()) {
            inChannel.transferTo(0, inChannel.size(), outChannel);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}

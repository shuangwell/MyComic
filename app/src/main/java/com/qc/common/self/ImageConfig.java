package com.qc.common.self;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qmuiteam.qmui.widget.QMUIProgressBar;

import java.util.Map;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/2/24 22:30
 * @ver 1.0
 */
public class ImageConfig {

    private String url;

    private Map<String, String> headers;

    private Object saveKey;

    private boolean isSave;

    private boolean isForce;

    private boolean isRead;

    private int defaultBitmapId;

    private int errorBitmapId;

    private int drawableId;

    public ImageConfig() {

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Object getSaveKey() {
        return saveKey;
    }

    public void setSaveKey(Object saveKey) {
        this.saveKey = saveKey;
    }

    public boolean isSave() {
        return isSave;
    }

    public void setSave(boolean save) {
        isSave = save;
    }

    public boolean isForce() {
        return isForce;
    }

    public void setForce(boolean force) {
        isForce = force;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public int getDefaultBitmapId() {
        return defaultBitmapId;
    }

    public void setDefaultBitmapId(int defaultBitmapId) {
        this.defaultBitmapId = defaultBitmapId;
    }

    public int getErrorBitmapId() {
        return errorBitmapId;
    }

    public void setErrorBitmapId(int errorBitmapId) {
        this.errorBitmapId = errorBitmapId;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public static class View {
        private RelativeLayout layout;

        private ImageView imageView;

        private QMUIProgressBar progressBar;

        private TextView textView;

        public RelativeLayout getLayout() {
            return layout;
        }

        public void setLayout(RelativeLayout layout) {
            this.layout = layout;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public void setImageView(ImageView imageView) {
            this.imageView = imageView;
        }

        public QMUIProgressBar getProgressBar() {
            return progressBar;
        }

        public void setProgressBar(QMUIProgressBar progressBar) {
            this.progressBar = progressBar;
        }

        public TextView getTextView() {
            return textView;
        }

        public void setTextView(TextView textView) {
            this.textView = textView;
        }
    }
}

package com.qc.common.ui.adapter;

import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.qc.common.self.ImageConfig;
import com.qc.common.util.ImageUtil;
import com.qc.common.util.SourceUtil;
import com.qc.mycomic.R;

import org.jetbrains.annotations.NotNull;

import the.one.base.adapter.TheBaseQuickAdapter;
import the.one.base.adapter.TheBaseViewHolder;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 11:45
 * @ver 1.0
 */
public class RankAdapter extends TheBaseQuickAdapter<Entity> {

    public static final int NO_IMG = -1;

    public RankAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(@NotNull TheBaseViewHolder holder, Entity entity) {
        EntityInfo info = entity.getInfo();
        holder.setText(R.id.tvTitle, info.getTitle());
        holder.setText(R.id.tvAuthor, info.getAuthor() != null ? info.getAuthor() : "作者未知");
        holder.setText(R.id.tvUpdateTime, info.getUpdateTime());
        holder.setGone(R.id.tvUpdateTime, info.getUpdateTime() == null);
        holder.setText(R.id.tvUpdateChapter, info.getUpdateChapter());
        holder.setGone(R.id.tvUpdateChapter, info.getUpdateChapter() == null);
        holder.setText(R.id.tvIndex, String.valueOf(getItemPosition(entity) + 1));
        RelativeLayout relativeLayout = holder.findView(R.id.imageRelativeLayout);
        if (relativeLayout != null) {
            ImageConfig config = ImageUtil.getDefaultConfig(info.getImgUrl());
            Source source = SourceUtil.getSource(entity.getSourceId());
            config.setHeaders(source.getImageHeaders());
            ImageUtil.loadImage(config, relativeLayout);
        }
    }

    @Override
    protected int getDefItemViewType(int position) {
        if (getData().get(position).getInfo().getImgUrl() == null) {
            return NO_IMG;
        }
        return super.getDefItemViewType(position);
    }

    @NotNull
    @Override
    protected TheBaseViewHolder onCreateDefViewHolder(@NotNull ViewGroup parent, int viewType) {
        if (viewType == NO_IMG) {
            return super.createBaseViewHolder(parent, R.layout.item_rank_right_simple);
        }
        return super.onCreateDefViewHolder(parent, viewType);
    }
}
package com.qc.common.ui.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qc.common.en.ValueEnum;
import com.qc.common.en.data.Data;
import com.qc.common.en.data.Text;
import com.qc.common.ui.adapter.ShelfAdapter;
import com.qc.common.ui.presenter.ShelfPresenter;
import com.qc.common.ui.view.ShelfView;
import com.qc.common.util.DBUtil;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.PopupUtil;
import com.qc.common.util.SourceUtil;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.dialog.QMUIBottomSheet;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.Request;
import the.one.base.ui.fragment.BaseListFragment;
import the.one.base.ui.presenter.BasePresenter;
import the.one.base.util.QMUIDialogUtil;
import the.one.base.util.ToastUtil;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.MapUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/9 18:05
 * @ver 1.0
 */
public class ShelfItemFragment extends BaseListFragment<Entity> implements ShelfView {

    private List<Entity> entityList;
    private ShelfPresenter presenter = new ShelfPresenter();
    private ShelfAdapter shelfAdapter = new ShelfAdapter();

    //判断显示收藏或历史数据
    private int status;
    private int column;

    //统计检查更新完成数量
    private int count;
    //统计检查更新失败数据
    private List<String> errorList = new ArrayList<>();

    String[] items = ValueEnum.SHELF_ITEM_CENTER.value();
    String[] screenItems = ValueEnum.SHELF_ITEM_SCREEN.value();

    public static ShelfItemFragment getInstance(int status) {
        ShelfItemFragment fragment = new ShelfItemFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("status", status);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        this.status = (int) getArguments().get("status");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (entityList == null) {
            entityList = EntityUtil.getEntityList(status);
        }
        if (adapter != null && !adapter.getData().isEmpty()) {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onLazyInit() {
        super.onLazyInit();
        presenter.preload(entityList);
    }

    @Override
    protected void initView(View rootView) {
        super.initView(rootView);
        showLoadingPage();
    }

    @Override
    protected int setType() {
        return TYPE_GRID;
    }

    @Override
    protected int setColumn() {
        if (column == 0) {
            int screenWidth = QMUIDisplayHelper.getScreenWidth(_mActivity);
            int height = QMUIDisplayHelper.dp2px(_mActivity, 160);
            int width = screenWidth;
            column = 1;
            while (width + 20 > height) {
                column++;
                width = screenWidth / column;
            }
        }
        return column;
    }

    @Override
    protected BaseQuickAdapter getAdapter() {
        return shelfAdapter;
    }

    @Override
    protected void initAdapter() {
        super.initAdapter();
        adapter.getLoadMoreModule().setOnLoadMoreListener(null);
    }

    @Override
    protected void requestServer() {
        if (entityList == null || entityList.isEmpty()) {
            entityList = EntityUtil.getEntityList(status);
            if (EntityUtil.getEntityList().isEmpty() && status == EntityUtil.STATUS_FAV) {
                ToastUtil.show(Text.TOAST_GO_TO_SEARCH);
                DBUtil.deleteShelfImg();
            }
            onFirstComplete(entityList);
        } else if (sList == shelfAdapter.getData()) {
            onFirstComplete(sList);
        } else if (entityList != EntityUtil.getEntityList(status)) {
            entityList = EntityUtil.getEntityList(status);
            onFirstComplete(entityList);
        } else {
            onFirstComplete(entityList);
        }
        recycleView.scrollBy(0, 0);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
        Entity entity = shelfAdapter.getItem(position);
        if (entity.isUpdate()) {
            entity.setUpdate(false);
            EntityUtil.first(entity);
        }
        entity.setPriority(0);
        DBUtil.save(entity, DBUtil.SAVE_ONLY);
        Data.setEntity(entity);
        startFragment(new ChapterFragment());
    }

    @Override
    public boolean onItemLongClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {
        Entity entity = shelfAdapter.getItem(position);
        QMUIDialogUtil.showMenuDialog(getContext(), Text.OPTION_TITLE, items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (which == 0) {
                    //查看信息
                    QMUIDialogUtil.showSimpleDialog(getContext(), Text.INFO, EntityUtil.toStringView(entity)).show();
                } else if (which == 1) {
                    //更改数据源
                    Map<String, String> map = PopupUtil.getMap(entity.getInfoList());
                    String key = PopupUtil.getKey(entity);
                    PopupUtil.showSimpleBottomSheetList(getContext(), map, key, Text.CHANGE_SOURCE, new QMUIBottomSheet.BottomListSheetBuilder.OnSheetItemClickListener() {
                        @Override
                        public void onClick(QMUIBottomSheet dialog, View itemView, int position, String tag) {
                            String key = MapUtil.getKeyByValue(map, tag);
                            String[] ss = key.split("#");
                            if (EntityUtil.changeInfo(entity, ss)) {
                                adapter.notifyDataSetChanged();
                                DBUtil.save(entity, DBUtil.SAVE_ONLY);
                            }
                            dialog.dismiss();
                        }
                    });
                } else if (which == 2) {
                    //删除数据
                    QMUIDialogUtil.showSimpleDialog(getContext(), Text.DELETE_TITLE, Text.DELETE_TIP, new QMUIDialogAction.ActionListener() {
                        @Override
                        public void onClick(QMUIDialog dialog, int index) {
                            shelfAdapter.remove(entity);
                            entityList.remove(entity);
                            EntityUtil.getEntityList().remove(entity);
                            DBUtil.deleteData(entity);
                            adapter.notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    }).show();
                }
            }
        }).show();
        return true;
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    /*-----------------------------------------------------------------------------------------*/
    //checkUpdate
    public void startCheckUpdate() {
        if (entityList.isEmpty()) {
            showFailTips(Text.TIP_NO_DATA);
            return;
        }
        if (presenter.runningCallsCount() == 0) {
            count = 0;
            errorList.clear();
        }
        if (count != 0 || infoCount != 0) {
            ToastUtil.show(Text.TOAST_LOAD_ING);
            progressDialog.show();
            return;
        }
        String msg = String.format(Locale.CHINA, Text.FORMAT_UPDATE_PROGRESS, count, entityList.size());
        initProgressDialog();
        showProgressDialog(count, entityList.size(), msg);
        presenter.checkUpdate(entityList);
    }

    @Override
    public void loadSourceComplete(String errorMsg, String json, String tag, Request request) {
        if (Source.DETAIL.equals(tag)) {
            if (errorMsg != null) {
                errorList.add(errorMsg);
            } else {
                adapter.notifyDataSetChanged();
            }
            if (++count == entityList.size()) {
                if (errorList.isEmpty()) {
                    hideProgressDialog();
                    showSuccessTips(Text.UPDATE_COMPLETE);
                } else {
                    StringBuilder tip = new StringBuilder();
                    for (String s : errorList) {
                        tip.append(s).append("\n");
                    }
                    String content = String.format(Locale.CHINA, Text.FORMAT_UPDATE_RESULT, errorList.size(), tip);
                    hideProgressDialog();
                    QMUIDialogUtil.showPositiveDialog(getContext(), Text.UPDATE_COMPLETE_RESULT, content, Text.OPTION_CANCEL, (dialog, index) -> {
                        dialog.dismiss();
                    }, Text.OPTION_RETRY, (dialog, index) -> {
                        dialog.dismiss();
                        startCheckUpdate();
                    });
                }
                count = 0;
                errorList.clear();
                presenter.preload(entityList);
            } else {
                String msg = String.format(Locale.CHINA, Text.FORMAT_UPDATE_PROGRESS, count, entityList.size());
                showProgressDialog(count, entityList.size(), msg);
            }
        } else if (Source.SEARCH.equals(tag)) {
            if (errorMsg != null) {
                errorList.add(errorMsg);
            }
            if (++infoCount == entityList.size()) {
                hideProgressDialog();
                if (errorList.isEmpty()) {
                    showSuccessTips(Text.UPDATE_COMPLETE);
                } else {
                    StringBuilder tip = new StringBuilder();
                    for (String s : errorList) {
                        tip.append(s).append("\n");
                    }
                    String content = String.format(Locale.CHINA, Text.FORMAT_ADD_RESULT, errorList.size(), tip);
                    QMUIDialogUtil.showSimpleDialog(getContext(), Text.ADD_RESULT, content);
                }
                infoCount = 0;
                errorList.clear();
            } else {
                String msg = String.format(Locale.CHINA, Text.FORMAT_ADD_PROGRESS, infoCount, entityList.size());
                showProgressDialog(infoCount, entityList.size(), msg);
            }
        }
    }

    /*-----------------------------------------------------------------------------------------*/
    //screen
    private List<Entity> sList = new ArrayList<>();

    public void screen(boolean isScreen) {
        if (entityList.isEmpty()) {
            showFailTips(Text.TIP_NO_DATA);
            return;
        }
        if (isScreen) {
            QMUIDialogUtil.showMenuDialog(getContext(), Text.OPTION_TITLE, screenItems, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (which == 0) {
                        sList.clear();
                        for (Entity entity : entityList) {
                            EntityInfo info = entity.getInfo();
                            if (info.getCurChapterTitle() == null || !info.getCurChapterTitle().equals(info.getUpdateChapter())) {
                                sList.add(entity);
                            }
                        }
                        onFirstComplete(sList);
                    } else if (which == 1) {
                        QMUIDialogUtil.showEditTextDialog(getContext(), Text.SCREEN_TITLE, Text.EMPTY, Text.TIP_INPUT_TITLE, new QMUIDialogUtil.OnEditTextConfirmClickListener() {
                            @Override
                            public void getEditText(QMUIDialog dialog, CharSequence content, int index) {
                                if (!content.toString().trim().equals("")) {
                                    sList.clear();
                                    for (Entity entity : entityList) {
                                        if (entity.getTitle().contains(content)) {
                                            sList.add(entity);
                                        }
                                    }
                                    onFirstComplete(sList);
                                }
                                dialog.dismiss();
                            }
                        }).show();
                    }
                }
            }).show();
        } else {
            if (shelfAdapter.getData() != entityList) {
                onFirstComplete(entityList);
            }
        }
    }

    /*-----------------------------------------------------------------------------------------*/
    //import
    public void importEntity() {
        QMUIDialogUtil.showEditTextDialog(getContext(), Text.INPUT_TITLE, Text.EMPTY, Text.TIP_INPUT_URL, new QMUIDialogUtil.OnEditTextConfirmClickListener() {
            @Override
            public void getEditText(QMUIDialog dialog, CharSequence content, int index) {
                try {
                    if (index == 0) {
                        dialog.dismiss();
                    } else {
                        Source source = null;
                        if (content != null) {
                            List<Source> list = SourceUtil.getSourceList();
                            for (Source s : list) {
                                String tmp = content.toString();
                                if (content.toString().contains("//m.") || content.toString().contains("//www.")) {
                                    if (s.getIndex().contains("//m.")) {
                                        tmp = tmp.replace("//www.", "//m.");
                                    } else if (s.getIndex().contains("//www.")) {
                                        tmp = tmp.replace("//m.", "//www.");
                                    }
                                }
                                if (tmp.startsWith(s.getIndex())) {
                                    source = s;
                                    content = tmp;
                                    break;
                                }
                            }
                        }
                        dialog.dismiss();
                        if (source != null) {
                            EntityInfo info = SourceUtil.getInfo();
                            info.setSourceId(source.getSourceId());
                            info.setDetailUrl(content.toString());
                            Entity entity = SourceUtil.getEntity(info);
                            entity.setPriority(0);
                            Data.setEntity(entity);
                            Data.toStatus = Data.TO_IMPORT;
                            startFragment(new ChapterFragment());
                        } else {
                            showFailTips(Text.TIP_URL_FAIL);
                        }
                    }
                } catch (Exception e) {
                    dialog.dismiss();
                    showFailTips(Text.TIP_URL_FAIL);
                }
            }
        });
    }

    /*-----------------------------------------------------------------------------------------*/
    private int infoCount = 0;

    //add
    public void addEntityInfo() {
        if (entityList.isEmpty()) {
            showFailTips(Text.TIP_NO_DATA);
            return;
        }
        List<String> list = SourceUtil.getSourceNameList();
        String[] items = new String[list.size()];
        items = list.toArray(items);
        QMUIDialogUtil.showMenuDialog(getContext(), Text.SOURCE, items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = list.get(which);
                int sourceId = SourceUtil.getSourceId(name);
                Source source = SourceUtil.getSource(sourceId);
                dialog.dismiss();
                if (presenter.runningCallsCount() == 0) {
                    infoCount = 0;
                    errorList.clear();
                }
                if (count != 0 || infoCount != 0) {
                    ToastUtil.show(Text.TOAST_LOAD_ING);
                    progressDialog.show();
                    return;
                }
                String msg = String.format(Locale.CHINA, Text.FORMAT_ADD_PROGRESS, infoCount, entityList.size());
                initProgressDialog();
                showProgressDialog(infoCount, entityList.size(), msg);
                presenter.addEntityInfo(entityList, source);
            }
        });
    }

}

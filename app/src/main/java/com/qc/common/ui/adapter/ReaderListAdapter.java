package com.qc.common.ui.adapter;

import android.content.res.ColorStateList;

import androidx.annotation.Nullable;

import com.qc.mycomic.R;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButtonDrawable;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundLinearLayout;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import the.one.base.adapter.TheBaseQuickAdapter;
import the.one.base.adapter.TheBaseViewHolder;
import top.luqichuang.common.model.ChapterInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/2/28 12:48
 * @ver 1.0
 */
public class ReaderListAdapter extends TheBaseQuickAdapter<ChapterInfo> {

    private int position = 0;

    public ReaderListAdapter(int layoutResId, @Nullable List<ChapterInfo> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(@NotNull TheBaseViewHolder holder, ChapterInfo chapterInfo) {
        holder.setText(R.id.tvTitle, chapterInfo.getTitle());
        QMUIRoundLinearLayout linearLayout = holder.getView(R.id.linearLayout);
        QMUIRoundButtonDrawable drawable = (QMUIRoundButtonDrawable) linearLayout.getBackground();
        if (position == getItemPosition(chapterInfo)) {
            holder.setTextColor(R.id.tvTitle, getColor(R.color.white));
            drawable.setBgData(ColorStateList.valueOf(getColor(R.color.blue)));
        } else {
            holder.setTextColor(R.id.tvTitle, getColor(R.color.black));
            drawable.setBgData(ColorStateList.valueOf(getColor(R.color.white)));
        }
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

}

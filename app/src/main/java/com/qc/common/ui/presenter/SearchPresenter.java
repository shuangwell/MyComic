package com.qc.common.ui.presenter;


import com.qc.common.ui.view.SearchView;
import com.qc.common.util.SourceUtil;

import java.util.List;

import okhttp3.Request;
import top.luqichuang.common.model.Source;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 15:33
 * @ver 1.0
 */
public class SearchPresenter extends SourcePresenter<SearchView> {

    public void search(String searchString) {
        newThread(() -> {
            List<Source> sourceList = SourceUtil.getSourceList();
            for (Source source : sourceList) {
                Request request = source.getSearchRequest(searchString);
                startLoad(request, source, Source.SEARCH);
            }
        });
    }

}

package com.qc.common.ui.presenter;

import com.qc.common.en.data.Data;
import com.qc.common.en.data.Text;
import com.qc.common.ui.view.ShelfView;
import com.qc.common.util.DBUtil;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.SourceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;

import okhttp3.Request;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.self.CommonCallback;
import top.luqichuang.common.util.NetUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/9 18:50
 * @ver 1.0
 */
public class ShelfPresenter extends SourcePresenter<ShelfView> {

    private int priority = 0;

    private final Set<String> requestSet = new HashSet<>();

    private final Map<Request, Entity> map = new HashMap<>();
    private final Map<Integer, Queue<Request>> queueMap = new HashMap<>();

    private static final int CACHE_NUM = 5;

    private boolean checkTime() {
        long time = System.currentTimeMillis();
        if (time - Data.time < 60 * 60 * 1000) {
            Data.time = time;
            return true;
        } else {
            Data.time = time;
            return false;
        }
    }

    public void checkUpdate(List<Entity> entityList) {
        newThread(() -> {
            priority = 0;
            if (!checkTime()) {
                requestSet.clear();
            }
            map.clear();
            queueMap.clear();
            List<Entity> list = new ArrayList<>(entityList);
            for (Entity entity : list) {
                int sourceId = entity.getInfo().getSourceId();
                Source source = SourceUtil.getSource(sourceId);
                Request request = source.getDetailRequest(entity.getInfo().getDetailUrl());
                map.put(request, entity);
                Queue<Request> queue = queueMap.get(sourceId);
                if (queue == null) {
                    queue = new LinkedList<>();
                    queueMap.put(sourceId, queue);
                }
                queue.offer(request);
            }
            for (Map.Entry<Integer, Queue<Request>> entry : queueMap.entrySet()) {
                int sourceId = entry.getKey();
                Queue<Request> queue = entry.getValue();
                int size = Math.min(queue.size(), CACHE_NUM);
                for (int i = 0; i < size; i++) {
                    checkUpdate(queue.poll(), sourceId);
                }
            }
        });
    }

    private void loadNext(int sourceId) {
        Queue<Request> queue = queueMap.get(sourceId);
        if (queue != null && !queue.isEmpty()) {
            checkUpdate(queue.poll(), sourceId);
        }
    }

    private void checkUpdate(Request request, int sourceId) {
        if (request == null) {
            return;
        }
        Entity entity = map.get(request);
        if (entity == null) {
            return;
        }
        if (requestSet.contains(request.toString())) {
            loadSourceEnd(null, null, Source.DETAIL, request);
            loadNext(sourceId);
            return;
        }
        EntityInfo info = entity.getInfo();
        info.getChapterInfoList().clear();
        info.getChapterInfoMap().clear();
        Source source = SourceUtil.getSource(sourceId);
        NetUtil.startLoad(request, new CommonCallback(source, Source.DETAIL) {
            @Override
            public void onFailure(String errorMsg) {
                String msg = String.format("[%s] %s\n%s\n", getSource().getSourceName(), info.getTitle(), errorMsg);
                loadSourceEnd(msg, null, getTag(), request);
                loadNext(sourceId);
            }

            @Override
            public void onResponse(String html, Map<String, Object> map) {
                String curUpdateChapter = info.getUpdateChapter();
                SourceUtil.setInfoDetail(source, info, html, map);
                if (curUpdateChapter == null || !curUpdateChapter.equals(info.getUpdateChapter())) {
                    if (info.getUpdateChapter() != null) {
                        entity.setUpdate(true);
                        entity.setPriority(++priority);
                        EntityUtil.first(entity);
                    }
                }
                if (info.getChapterInfoList().isEmpty()) {
                    onFailure(Text.TIP_NO_DATA);
                } else {
                    DBUtil.saveCache(request.toString(), info);
                    requestSet.add(request.toString());
                    loadSourceEnd(null, null, getTag(), request);
                    loadNext(sourceId);
                }
                DBUtil.save(entity, DBUtil.SAVE_CUR);
            }
        });
    }

    public void addEntityInfo(List<Entity> entityList, Source source) {
        newThread(() -> {
            for (Entity entity : entityList) {
                boolean exist = false;
                for (EntityInfo info : entity.getInfoList()) {
                    if (info.getSourceId() == source.getSourceId()) {
                        exist = true;
                        break;
                    }
                }
                if (exist) {
                    loadSourceEnd(null, null, Source.SEARCH);
                    continue;
                }
                String title = entity.getTitle();
                Request request = source.getSearchRequest(title);
                NetUtil.startLoad(request, new CommonCallback(source, Source.SEARCH) {
                    @Override
                    public void onFailure(String errorMsg) {
                        String msg = String.format("%s # %s", title, errorMsg);
                        loadSourceEnd(msg, null, getTag(), request);
                    }

                    @Override
                    public void onResponse(String html, Map<String, Object> map) {
                        List<EntityInfo> infoList = SourceUtil.getInfoList(source, html);
                        boolean exist = false;
                        for (EntityInfo info : infoList) {
                            if (Objects.equals(title, info.getTitle())) {
                                EntityUtil.addInfo(entity, info);
                                exist = true;
                                DBUtil.saveInfoData(info);
                                break;
                            }
                        }
                        if (exist) {
                            loadSourceEnd(null, null, getTag(), request);
                        } else {
                            onFailure(Text.TIP_NO_DATA);
                        }
                    }
                });
            }
        });
    }

}

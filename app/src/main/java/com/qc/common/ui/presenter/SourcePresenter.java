package com.qc.common.ui.presenter;

import com.qc.common.ui.view.SourceView;
import com.qc.common.util.DBUtil;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.SourceUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.model.CacheData;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.self.CommonCallback;
import top.luqichuang.common.util.CacheUtil;
import top.luqichuang.common.util.NetUtil;

/**
 * @author 18472
 * @desc
 * @date 2024/3/22 18:17
 * @ver 1.0
 */
public class SourcePresenter<V extends SourceView> extends MyPresenter<V> {

    public void loadSourceEnd(String errorMsg, String json, String tag) {
        loadSourceEnd(errorMsg, json, tag, null);
    }

    public void loadSourceEnd(String errorMsg, String json, String tag, Request request) {
        mainThread(() -> {
            if (isViewAttached()) {
                getView().loadSourceComplete(errorMsg, json, tag, request);
            }
        });
    }

    protected void startLoad(Request request, Source source, String tag) {
        startLoad(request, source, tag, 0);
    }

    protected void startLoad(Request request, Source source, String tag, int chapterId) {
        startLoad(request, source, tag, chapterId, false);
    }

    protected void startLoad(Request request, Source source, String tag, int chapterId, boolean isPreload) {
        String json = getCacheJson(request);
        if (json != null) {
            if (!isPreload) {
                loadSourceEnd(null, json, tag, request);
            }
            return;
        }
        NetUtil.startLoad(request, new CommonCallback(source, tag) {
            @Override
            protected void initData(Map<String, Object> data) {
                super.initData(data);
                data.put("chapterId", chapterId);
            }

            @Override
            protected int getReloadNum() {
                if (isPreload) {
                    return 3;
                }
                if (Source.CONTENT.equals(getTag())) {
                    return 3;
                }
                return 1;
            }

            @Override
            public void onFailure(String errorMsg) {
                String msg = errorMsg;
                if (Source.SEARCH.equals(getTag())) {
                    msg = String.format("[%s] %s", getSource().getSourceName(), errorMsg);
                }
                if (!isPreload) {
                    loadSourceEnd(msg, null, getTag(), request);
                }
                DBUtil.deleteCache(request.toString());
            }

            @Override
            public void onResponse(String html, Map<String, Object> map) {
                String json = null;
                if (Source.SEARCH.equals(getTag())) {
                    List<EntityInfo> infoList = SourceUtil.getInfoList(source, html);
                    json = CacheUtil.toJson(infoList);
                } else if (Source.DETAIL.equals(getTag())) {
                    EntityInfo entityInfo = SourceUtil.getInfo();
                    SourceUtil.setInfoDetail(getSource(), entityInfo, html, map);
                    CacheData cacheData = DBUtil.saveCache(request.toString(), entityInfo);
                    json = cacheData.getJson();
                } else if (Source.CONTENT.equals(getTag())) {
                    List<Content> contentList = SourceUtil.getContentList(getSource(), html, chapterId, map);
                    if (isPreload && contentList.isEmpty()) {
                        return;
                    }
                    CacheData cacheData = DBUtil.saveCache(request.toString(), contentList);
                    json = cacheData.getJson();
                } else if (Source.RANK.equals(getTag())) {
                    List<EntityInfo> infoList = SourceUtil.getRankInfoList(source, html);
                    CacheData cacheData = DBUtil.saveCache(request.toString(), infoList);
                    json = cacheData.getJson();
                }
                if (!isPreload) {
                    loadSourceEnd(null, json, getTag(), request);
                }
            }
        }, isPreload);
    }

    public void preload(List<Entity> entityList) {
        newThread(() -> {
            List<Entity> list = new ArrayList<>(entityList);
            for (Entity entity : list) {
                EntityInfo info = entity.getInfo();
                if (info.getChapterInfoList().isEmpty()) {
                    Source source = SourceUtil.getSource(info.getSourceId());
                    Request request = source.getDetailRequest(info.getDetailUrl());
                    String json = getCacheJson(request);
                    if (json != null) {
                        EntityInfo cacheInfo = CacheUtil.toObject(json, SourceUtil.getEntityInfoClass());
                        EntityUtil.updateInfo(info, cacheInfo);
                    }
                }
                if (!info.getChapterInfoList().isEmpty()) {
                    int curChapterId = info.getCurChapterId();
                    int nextChapterId = curChapterId + 1;
                    int lastChapterId = info.getChapterInfoList().size() - 1;
                    loadContent(info, curChapterId, true);
                    loadContent(info, nextChapterId, true);
                    loadContent(info, lastChapterId, true);
                }
            }
        });
    }

    public void loadChapter(Entity entity) {
        EntityInfo info = entity.getInfo();
        Source source = SourceUtil.getSource(info.getSourceId());
        Request request = source.getDetailRequest(info.getDetailUrl());
        startLoad(request, source, Source.DETAIL);
    }

    public void loadContent(EntityInfo info, int chapterId, boolean isPreload) {
        List<ChapterInfo> chapterInfoList = info.getChapterInfoList();
        if (EntityUtil.checkChapterId(info, chapterId)) {
            int position = EntityUtil.getPosition(info, chapterId);
            String chapterUrl = chapterInfoList.get(position).getChapterUrl();
            Source source = SourceUtil.getSource(info.getSourceId());
            Request request = source.getContentRequest(chapterUrl);
            startLoad(request, source, Source.CONTENT, chapterId, isPreload);
        }
    }

    public List<Entity> mergeInfoList(List<EntityInfo> infoList) {
        List<Entity> entityList = new ArrayList<>();
        if (infoList != null) {
            for (EntityInfo entityInfo : infoList) {
                boolean isExists = false;
                for (Entity entity : entityList) {
                    if (entity.getTitle().equals(entityInfo.getTitle())) {
                        isExists = true;
                        EntityUtil.addInfo(entity, entityInfo);
                    }
                }
                if (!isExists) {
                    Entity entity = SourceUtil.getEntity(entityInfo);
                    entityList.add(entity);
                }
            }
        }
        return entityList;
    }

    public List<Entity> getEntityList(List<EntityInfo> infoList) {
        List<Entity> entityList = new ArrayList<>();
        if (infoList != null) {
            for (EntityInfo info : infoList) {
                if (info.getTitle() != null) {
                    Entity entity = SourceUtil.getEntity(info);
                    entityList.add(entity);
                }
            }
        }
        return entityList;
    }

    public int runningCallsCount() {
        return NetUtil.runningCallsCount();
    }

    public int queuedCallsCount() {
        return NetUtil.queuedCallsCount();
    }

}

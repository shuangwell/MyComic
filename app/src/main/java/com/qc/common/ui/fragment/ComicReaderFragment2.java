package com.qc.common.ui.fragment;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qc.common.ui.adapter.ComicReaderAdapter2;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;

import the.one.base.Interface.IPageInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/9/1 19:09
 * @ver 1.0
 */
public class ComicReaderFragment2 extends ComicReaderFragment {

    protected ComicReaderAdapter2 readerAdapter2 = new ComicReaderAdapter2();
    protected PagerSnapHelper mPagerSnapHelper;
    protected LinearLayoutManager mPagerLayoutManager;

    protected int getOrientation() {
        return RecyclerView.HORIZONTAL;
    }

    @Override
    protected BaseQuickAdapter getAdapter() {
        return readerAdapter2;
    }

    @Override
    protected void initRecycleView(RecyclerView recycleView, int type, BaseQuickAdapter adapter) {
        super.initRecycleView(recycleView, type, adapter);
        mPagerSnapHelper = new PagerSnapHelper();
        mPagerLayoutManager = new LinearLayoutManager(_mActivity, getOrientation(), false);
        recycleView.setLayoutManager(mPagerLayoutManager);
        mPagerSnapHelper.attachToRecyclerView(recycleView);
    }

    @Override
    public void setPageInfo(IPageInfo mPageInfo) {
        super.setPageInfo(mPageInfo);
        setPullLayoutEnabled(false);
    }

    @Override
    protected boolean checkMenuClick(int position) {
        int width = QMUIDisplayHelper.getScreenWidth(_mActivity);
        int length = width / 3;
        if (touchX < length) {
            if (checkPosition(position - 1)) {
                recycleView.scrollToPosition(position - 1);
            }
        } else if (touchX > length * 2) {
            if (checkPosition(position + 1)) {
                recycleView.scrollToPosition(position + 1);
            }
        } else {
            return true;
        }
        return false;
    }
}

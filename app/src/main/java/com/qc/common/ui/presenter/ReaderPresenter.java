package com.qc.common.ui.presenter;

import com.qc.common.ui.view.ReaderView;

import top.luqichuang.common.model.Entity;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 22:30
 * @ver 1.0
 */
public class ReaderPresenter extends SourcePresenter<ReaderView> {

    public void load(Entity entity) {
        int chapterId = entity.getInfo().getCurChapterId();
        load(entity, chapterId);
    }

    public void load(Entity entity, int chapterId) {
        newThread(() -> {
            loadContent(entity.getInfo(), chapterId, false);
        });
    }

}

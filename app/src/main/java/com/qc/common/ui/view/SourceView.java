package com.qc.common.ui.view;

import okhttp3.Request;
import the.one.base.ui.view.BaseView;

/**
 * @author 18472
 * @desc
 * @date 2024/3/22 18:18
 * @ver 1.0
 */
public interface SourceView extends BaseView {

    void loadSourceComplete(String errorMsg, String json, String tag, Request request);

}

package com.qc.common.en;

import com.qc.common.en.data.Data;

import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.en.NSourceEnum;
import top.luqichuang.common.en.VSourceEnum;
import top.luqichuang.common.model.comic.Comic;
import top.luqichuang.common.model.comic.ComicInfo;
import top.luqichuang.common.model.novel.Novel;
import top.luqichuang.common.model.novel.NovelInfo;
import top.luqichuang.common.model.video.Video;
import top.luqichuang.common.model.video.VideoInfo;

/**
 * @author 18472
 * @desc
 * @date 2024/2/19 16:49
 * @ver 1.0
 */
public enum ValueEnum {

    HOME_TAB_BARS(new String[]{"我的画架", "搜索漫画", "个人中心"},
            new String[]{"我的书架", "搜索小说", "个人中心"},
            new String[]{"我的番剧", "搜索番剧", "个人中心"}),
    SHELF_TAB_BARS(new String[]{"收藏漫画", "历史漫画"},
            new String[]{"收藏小说", "历史小说"},
            new String[]{"收藏番剧", "历史番剧"}),
    SHELF_MENUS(new String[]{"检查更新", "筛选漫画", "导入漫画", "批量添加源"},
            new String[]{"检查更新", "筛选小说", "导入小说", "批量添加源"},
            new String[]{"检查更新", "筛选番剧", "导入番剧", "批量添加源"}),
    SHELF_ITEM_CENTER(new String[]{"1、查看信息", "2、切换漫画源", "3、删除漫画"},
            new String[]{"1、查看信息", "2、切换小说源", "3、删除小说"},
            new String[]{"1、查看信息", "2、切换番剧源", "3、删除番剧"}),
    SHELF_ITEM_SCREEN(new String[]{"未读完漫画", "据标题筛选"},
            new String[]{"未读完小说", "据标题筛选"},
            new String[]{"未读完番剧", "据标题筛选"}),
    CHAPTER_MENUS(new String[]{"更新漫画源", "查看信息", "访问源网站", "切换章节显示"},
            new String[]{"更新小说源", "查看信息", "访问源网站", "切换章节显示"},
            new String[]{"更新番剧源", "查看信息", "访问源网站", "切换章节显示"}),

    SE_SOURCE_OPEN(SettingEnum.COMIC_SOURCE_OPEN, SettingEnum.NOVEL_SOURCE_OPEN, SettingEnum.VIDEO_SOURCE_OPEN),
    SE_SOURCE_TOTAL(SettingEnum.COMIC_SOURCE_TOTAL, SettingEnum.NOVEL_SOURCE_TOTAL, SettingEnum.VIDEO_SOURCE_TOTAL),
    SE_DEFAULT_SOURCE(SettingEnum.DEFAULT_COMIC_SOURCE, SettingEnum.DEFAULT_NOVEL_SOURCE, SettingEnum.DEFAULT_VIDEO_SOURCE),

    SOURCE_MAP(CSourceEnum.getMAP(), NSourceEnum.getMAP(), VSourceEnum.getMAP()),
    CLASS_ENTITY(Comic.class, Novel.class, Video.class),
    CLASS_ENTITY_INFO(ComicInfo.class, NovelInfo.class, VideoInfo.class),
    ;

    private final Object COMIC_VALUE;
    private final Object NOVEL_VALUE;
    private final Object VIDEO_VALUE;

    ValueEnum(Object COMIC_VALUE, Object NOVEL_VALUE, Object VIDEO_VALUE) {
        this.COMIC_VALUE = COMIC_VALUE;
        this.NOVEL_VALUE = NOVEL_VALUE;
        this.VIDEO_VALUE = VIDEO_VALUE;
    }

    public <T> T value() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return (T) COMIC_VALUE;
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return (T) NOVEL_VALUE;
        } else if (Data.contentCode == Data.VIDEO_CODE) {
            return (T) VIDEO_VALUE;
        } else {
            return null;
        }
    }
}

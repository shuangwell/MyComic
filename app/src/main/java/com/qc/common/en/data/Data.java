package com.qc.common.en.data;

import android.os.Build;

import com.qc.common.MyBaseApplication;
import com.qc.common.en.SettingEnum;
import com.qc.common.util.RestartUtil;

import java.io.File;
import java.util.List;

import the.one.base.util.SdCardUtil;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/10/30 11:48
 * @ver 1.0
 */
public class Data {

    /*=============================================================================*/
    /* 常量 */
    /* App Path */
    public static final String SD_CARD_PATH = SdCardUtil.getNormalSDCardPath();
    public static final String DIR_APP_NAME = "/MyComic";
    public static final String DIR_IMG = "/Image";
    public static final String DIR_AUTO_SAVE = "/AutoBackup";
    public static final String DIR_SAVE = "/backup_comic.db";
    public static final String DB_FILE_NAME = "comic.db";
    public static final String SP_SAVE_STR = "json";

    /* 标识App显示的数据内容 */
    public static final int COMIC_CODE = 1;
    public static final int NOVEL_CODE = 2;
    public static final int VIDEO_CODE = 3;

    /* toStatus */
    public static final int NORMAL = 0;
    public static final int READER_TO_CHAPTER = 1;
    public static final int SEARCH_TO_CHAPTER = 2;
    public static final int RANK_TO_CHAPTER = 3;
    public static final int TO_IMPORT = 4;
    public static final int TO_IMPORT_SUCCESS = 5;

    /* SCREEN */
    public static final int SCREEN_0 = 0;
    public static final int SCREEN_1 = 1;

    /* ReaderMode */
    public static final int READER_MODE_V = 0;
    public static final int READER_MODE_H_R = 1;
    public static final int READER_MODE_H_L = 2;

    /*=============================================================================*/
    /* 公共静态变量 */
    public static int toStatus = Data.NORMAL;
    public static boolean isLight = true;
    public static boolean isFull = SettingEnum.IS_FULL_SCREEN.value();
    public static boolean isGrid = SettingEnum.IS_GRID.value();
    public static boolean isCache = SettingEnum.IS_CACHE.value();
    public static int contentCode = SettingEnum.READ_CONTENT.value();
    public static String contentStr = SettingEnum.READ_CONTENT.valueDesc();
    public static int videoSpeed = 2;
    public static long time = 0;

    /*=============================================================================*/
    /* 私有静态变量 */
    private static String appPath;
    private static Entity entity;
    private static Content content;
    private static List<ChapterInfo> chapterInfoList;

    public static Entity getEntity() {
        if (entity == null) {
            RestartUtil.restart();
        }
        return entity;
    }

    public static Content getContent() {
        if (content == null) {
            RestartUtil.restart();
        }
        return content;
    }

    public static List<ChapterInfo> getChapterInfoList() {
        if (chapterInfoList == null) {
            RestartUtil.restart();
        }
        return chapterInfoList;
    }

    public static void setEntity(Entity entity) {
        Data.entity = entity;
    }

    public static void setContent(Content content) {
        Data.content = content;
    }

    public static void setChapterInfoList(List<ChapterInfo> chapterInfoList) {
        Data.chapterInfoList = chapterInfoList;
    }
    /*=============================================================================*/

    public static String getAppPath() {
        if (appPath == null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                appPath = SD_CARD_PATH + DIR_APP_NAME;
            } else {
                File file = MyBaseApplication.getInstance().getExternalCacheDir();
                if (file != null) {
                    appPath = file.getParent();
                }
            }
        }
        return appPath;
    }

    public static String getImgPath() {
        return getAppPath() + DIR_IMG;
    }

    public static String getAutoSavePath() {
        return getAppPath() + DIR_AUTO_SAVE;
    }

    public static String getSavePathName() {
        return getAppPath() + DIR_SAVE;
    }

}
